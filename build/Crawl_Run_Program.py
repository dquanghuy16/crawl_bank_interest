
import camelot
import numpy as np
import ssl
from IPython.display import display
import cx_Oracle
from logging import exception
from sys import excepthook
from selenium import webdriver
import urllib3
import re
import time
import pandas as pd
import bs4 as bs
from collections import defaultdict
from selenium.webdriver.chrome.options import Options
from itertools import product
from selenium.common.exceptions import NoSuchElementException
from os import name
from sys import path
import urllib.request
from datetime import date
from http.client import IncompleteRead
import os

##### Một vài lỗi thường gặp
### 1. ORA-00001: unique constraint : là trùng khóa chính tức là đã có dữ liệu trong bảng có khóa chính cần truncate hoặc dùng update hoặc tắt lệnh cur.execute
### 2. Message: no such element: Unable to locate element : là không tìm ra element trên web vì web load chưa được cần tăng time.sleep lên vài chục giây hoặc trang web cập nhật mới cần tìm lại
### 3. Một số lỗi khác dư dấu .,()"@$%#^"... thì cần check lại nó sẽ báo là NOT DEFINE 


#### ----- Note ------- #####
#### 1. có 1 # là comment mở ra để chạy câu lệnh đó
#### 2. có 2 ## là comment chú thích
#### 3. có nhiều ### là 1 khu vực cũng để chú thích
#### 4. câu thực thi #********# để ghi vào database


### PDF
def list_path_pdf(self):
    pdf_acb = "D:/HuyDq/crawl/1_acb.pdf"
    pdf_sacombank = "D:/HuyDq/crawl/2_sacombank.pdf"
    pdf_techcombank = "D:/HuyDq/crawl/3_techcombank.pdf"
    pdf_vpbank = "D:/HuyDq/crawl/4_vpbank.pdf"
    pdf_hdbank = "D:/HuyDq/crawl/5_hdbank.pdf"
    pdf_vietbank = "D:/HuyDq/crawl/6_vietbank.pdf"
    pdf_shb = "D:/HuyDq/crawl/7_shb.pdf"
    pdf_hsbc = "D:/HuyDq/crawl/8_hsbc.pdf"

#### IMAGE
def list_url_anh(self):
    url_dongabank = "https://www.dongabank.com.vn/upload/lib/tttt-vnd.pdf?=1636360213"
    url_vietabank = "https://www.vietabank.com.vn/uploads/files/truy-cap-nhanh/lai-suat-tien-gui/ls1-12.08.2021.jpg"
    url_shb = "https://www.shb.com.vn/wp-content/uploads/2020/08/01.Bieu-LS-H%C4%90-KHCN-VND-11.10.2021-final.pdf"
    url_sc = "https://av.sc.com/vn/content/docs/vn-interest-rate-personal-banking-vn.pdf"

### big list pdf bank bao gồm bank và nhũng thuộc tính đi kèm: mô tả bên dưới
bank_pdf = [
    ['79307001'                             ## mã ngân hàng
    ,'https://acb.com.vn/wps/wcm/connect/6b0ba300-084f-4133-8773-58040a2863a9/Bang%2Btong%2Bhop%2Blai%2Bsuat_161021+%281%29.pdf?MOD=AJPERES'             ## đường dẫn đến pdf
    ,'PDF'                                  ## kiểu là pdf
    ,'1'                                    ## số thự tự trang pdf cần tách
    ,'0'                                    ## số thự tự bảng cần lấy data
    ,'Acb'],                                ## tên ngân hàng để lưu tên sheet 

    ['79303001'
    ,'https://www.sacombank.com.vn/canhan/Documents/LaiSuat/LAI_SUAT_NIEM_YET_TAI_QUAY_19.10.2021.pdf'
    ,'PDF'
    ,'1'
    ,'0'
    ,'Sacomank'],

    ['79310001'
    ,'https://www.techcombank.com.vn/file/get/24755'
    ,'PDF'
    ,'1'
    ,'0'
    ,'Techcombank'],

    ['79309001'
    ,'https://www.vpbank.com.vn/-/media/vpbank-latest/tai-lieu-bieu-mau/lai-suat-huy-dong/biu-li-sut--ti-quy-2021-07-03.pdf'
    ,'PDF'
    ,'1'
    ,'0'
    ,'Vpbank'],

    ['79321001'
    ,'https://hdbank.com.vn/api/download?fileName=document//20210824BIEULAISUATTIENGUIKHACHHANGCANHANfinal__1629964177938.pdf'
    ,'PDF'
    ,'1'
    ,'0'
    ,'Hdbank'],

    # ['79356001'
    # ,'D:/HuyDq/CRL_BANK_RATE/pdf/6_vietbank.pdf'
    # ,'PDF'
    # ,'1'
    # ,'0'
    # ,'Vietbank'],

    ['79348001'
    ,'https://www.shb.com.vn/wp-content/uploads/2020/08/01.Bieu-LS-H%C4%90-KHCN-VND-17.11.2021-final-1.pdf'
    ,'PDF'
    ,'1'
    ,'1'
    ,'Shb'],

    ['79617001'
    ,'https://www.hsbc.com.vn/content/dam/hsbc/hbvn/documents/vi/rates/HSBC_INTEREST_RATES_VN.pdf'
    ,'PDF'
    ,'1'
    ,'0'
    ,'HSBC']
    ]

### big list web bank bao gồm bank và nhũng thuộc tính đi kèm: mô tả bên dưới
bank_web =  [
    ['79319001'                                                             ## mã ngân hàng
    ,'http://oceanbank.vn/lai-suat/19/tiet-kiem-thuong.html'                ## đường dẫn đến trang web
    ,'Web'                                                                  ## kiểu là web
    ,'//*[@id="p-rate"]/div/div/div[2]/div/div[2]/div/table'                ## xpath đến table
    ,'0'                                                                    ## có thao tác trên trang web 1 là có, 0 là không
    ,'Oceanbank'],                                                          ## tên ngân hàng

    ['01204001'
    ,'https://www.agribank.com.vn/vn/lai-suat'
    ,'Web'
    ,'//*[@id="laiSuatCn"]/table'
    ,'0'
    ,'Agribank'],

    ['79302001'
    ,'https://www.msb.com.vn/Bieu-mau-Bieu-phi-va-Lai-suat/1177/3/Lai-suat'
    ,'Web'
    ,'//*[@id="table3"]'
    ,'0'
    ,'MSB'],

    ['79357001'
    ,'https://lienvietpostbank.com.vn/lai-suat'
    ,'Web'
    ,'//*[@id="Wrapper"]/div[2]/div[3]/div[1]/div[1]/table[2]'
    ,'0'
    ,'Lienvietpostbank'],

    ['79333001'
    ,'https://www.ocb.com.vn/vi/cong-cu/lai-suat'
    ,'Web'
    ,'/html/body/app-root/app-master/main/app-lai-suat/div/div/div/div[2]/div[3]/div[2]/table'
    ,'0'
    ,'Ocb'
    ],

    ['79320001'
    ,'https://www.gpbank.com.vn/InterestDetail'
    ,'Web'
    ,'//*[@id="divInterestDetail1"]/div[2]/table'  
    ,'0'
    ,'Gpbank'],

    ['970444'
    ,'https://www.cbbank.vn/Pages/InterestRate.aspx'
    ,'Web'
    ,'//*[@id="ContentPlaceHolder1_Panel1"]/div[1]/div/div[2]/div[1]/table'
    ,'0'
    ,'Cbbank'],

    ['79353001'
    ,'https://laisuat.kienlongbank.com/lai-suat-ca-nhan'
    ,'Web'
    ,'//*[@id="ctl00_mainContent_ctl02_up"]/div/div/section/article/div/table[2]'
    ,'0'
    ,'Kienlongbank'],

    ['79341001'
    ,'https://www.pgbank.com.vn/lai-suat-tiet-kiem/ca-nhan-vnd'
    ,'Web'
    ,'//*[@id="__layout"]/div/div[2]/div[3]/div[2]/div/table'
    ,'0'
    ,'Pgbank'],

    ['970415'
    ,'https://www.vietinbank.vn/web/home/vn/lai-suat'
    ,'Web'
    ,'//*[@id="hor-ex-b"]'
    ,'0'
    ,'Vietinbank'],

    ['79327001'
    ,'https://www.vietcapitalbank.com.vn/personal'
    ,'Web'
    ,'//*[@id="tool-table2"]/div[3]/div/div[1]/div/table'
    ,'0'
    ,'Vietcapitalbank'],

    ['01202001'
    ,'https://www.bidv.com.vn/vn/tra-cuu-lai-suat'
    ,'Web'
    ,'//*[@id="rates"]/div/table'
    ,'0'
    ,'Bidv'],

    ['79352001'
    ,'https://www.ncb-bank.vn/vi/lai-suat-tien-gui'
    ,'Web'
    ,'//*[@id="tab-laisuat"]/div/div[2]/div[1]/table[1]'
    ,'0'
    ,'Ncb'],

    ['79203001'
    ,'https://portal.vietcombank.com.vn/LS/Pages/lai-suat.aspx?devicechannel=default'
    ,'Web'
    ,'//*[@id="danhsachlaisuat"]'
    ,'0'
    ,'Vietcombank'],

    ['79306001'
    ,'https://www.namabank.com.vn/lai-suat'
    ,'Web'
    ,'//*[@id="ctl00_divCenter"]/div[2]/div/section/div/div[3]/div/table'
    ,'0'
    ,'NamA'],

    ['79359001'
    ,'https://www.baovietbank.vn/vn/lai-suat'
    ,'Web'
    ,'//*[@id="colap0"]/div/div[1]/table'
    ,'0'
    ,'Baovietbank'],

    ['01313007'
    ,'https://www.baca-bank.vn/SitePages/website/lai-xuat.aspx?ac=L%u00e3i+su%u1ea5t&s=LX'
    ,'Web'
    ,'/html/body'
    ,'1'
    ,'BacA'
    ,'//*[@id="ctl00_PlaceHolderMain_g_3d987530_2758_4587_b052_bda8fbe88390_ctl00_DDL_LS"]'
    ,'//*[@id="ctl00_PlaceHolderMain_g_3d987530_2758_4587_b052_bda8fbe88390_ctl00_DDL_LS"]/option[2]'],

    ['79360001'
    ,'https://www.pvcombank.com.vn/bieu-lai-suat'
    ,'Web'
    ,'/html/body'
    ,'1'
    ,'Pvcombank'
    ,'//*[@id="slSavingProduct"]'
    ,'//*[@id="slSavingProduct"]/option[5]'
    ,'//*[@id="savingProductName"]'],

    ['79308001'
    ,'https://www.saigonbank.com.vn/vi/truy-cap-nhanh/lai-suat/Lai-suat-tien-gui-tiet-kiem'
    ,'Web'
    ,'//*[@id="main-container"]/div/div[3]/div[1]/div/div[2]/div[4]/div[4]/div[3]/table'
    ,'0'
    ,'Saigonbank'],

    ['79358001'
    ,'https://tpb.vn/cong-cu-tinh-toan/lai-suat'
    ,'Web'
    ,'//*[@id="tab-1"]/table[1]'
    ,'0'
    ,'Tpbank']
    ]



opener = urllib.request.URLopener()
opener.addheader('User-Agent', 'whatever')
listt=[]
def download_file_pdf():
    today = date.today()
    pdf_date= today.strftime("%d/%m/%Y")
    pdf_date=str(pdf_date).replace("/","_")
    global listt

    leng =len(bank_pdf)                     ## lấy dộ dài của cả danh sách bank

    try:
        for i in range(leng):               ## chạy vòng for
            MASTER = bank_pdf[i]                  ## lấy ra từng bank để thực thi          
            MA_NH = MASTER[0]                 
            URL = MASTER[1]                   
            TYPE = MASTER[2]
            NAME = MASTER[5]
            PATH_DESC = F'D:/HuyDq/CRL_BANK_RATE/pdf/{NAME}_{pdf_date}.pdf'
            listt.append(PATH_DESC)
            print(PATH_DESC)
            filename, headers = opener.retrieve(f'{URL}',f'{PATH_DESC}')

            #urllib.request.urlretrieve(f'{URL}',f'{PATH_DESC}')
    except IncompleteRead:
        if len(listt)>0:
            for i in range(len(listt)):
                os.remove(f'{listt[i]}')
            listt=[]
            for i in range(leng):               ## chạy vòng for
                MASTER = bank_pdf[i]                  ## lấy ra từng bank để thực thi          
                MA_NH = MASTER[0]                 
                URL = MASTER[1]                   
                TYPE = MASTER[2]
                NAME = MASTER[5]
                PATH_DESC = F'D:/HuyDq/CRL_BANK_RATE/pdf/{NAME}_{pdf_date}.pdf'
                listt.append(PATH_DESC)
                #print(PATH_DESC)
                filename, headers = opener.retrieve(f'{URL}',f'{PATH_DESC}')

                #urllib.request.urlretrieve(f'{URL}',f'{PATH_DESC}')
                continue
        else:
            
            for i in range(leng):               ## chạy vòng for
                MASTER = bank_pdf[i]                  ## lấy ra từng bank để thực thi          
                MA_NH = MASTER[0]                 
                URL = MASTER[1]                   
                TYPE = MASTER[2]
                NAME = MASTER[5]
                PATH_DESC = F'D:/HuyDq/CRL_BANK_RATE/pdf/{NAME}_{pdf_date}.pdf'
                listt.append(PATH_DESC)
                #print(PATH_DESC)
                filename, headers = opener.retrieve(f'{URL}',f'{PATH_DESC}')

                #urllib.request.urlretrieve(f'{URL}',f'{PATH_DESC}')
                continue

def main_pdf():
    ########################### KHU VUC LOAD DATA VAO DB ######################
    
    # global check_compare            ## mặc định khi có lỗi là được gán bằng chuỗi 'error' khi truyền vào sp_log thì so sánh vs check_point không khớp thì ghi lỗi, khớp thì ko l
    # global error                    ## biến này khi xảy ra lỗi sẽ báo lỗi ở bank nào và thuộc kiểu crawl dạng nào
    # global result                   ## nếu thành công thì print "write successful"
    # global check_point              ## điểm check nếu có lỗi
    # global Err_sys                  ## ghi nhận lỗi hệ thống để show ra

    check_compare = ''
    error = ''
    result = ''
    Err_sys = ''
    result = '' 
    connection = cx_Oracle.connect('dwprod/oracle123@192.168.49.32:1521/uatdwh')  
    print("Database version:", connection.version)
    cur = connection.cursor()

    #trun_master='truncate table test_crawl_huy_master'
    #trun_action='truncate table test_crawl_huy_action'

    #cur.execute(trun_master)
    #cur.execute(trun_action)


    ### Tạo sẵn câu sql insert các thuộc tính vào bảng -> sau này sẽ xét thay đổi cập nhật chứ không phải insert 
    sql_master='INSERT INTO test_crl_huy_master (MA_NH,URL,TYPE,ATTRIBUTE,ACTION,PAGE,TABLE_NUM) VALUES(:1,:2,:3,:4,:5,:6,:7)'



    ####### Khu vực xử lý data

    leng =len(bank_pdf)                     ## lấy dộ dài của cả danh sách bank
    
    try:
        for i in range(leng):               ## chạy vòng for
            check_point = 2

            MASTER = bank_pdf[i]                  ## lấy ra từng bank để thực thi          
            MA_NH = MASTER[0]                 
            URL = listt[i]                   
            TYPE = MASTER[2]
            PAGE = MASTER[3]
            TABLE_NUM = int(MASTER[4])
            BANK_NAME = MASTER[5]

            VALUE_MASTER = [MA_NH,URL,TYPE,'','',PAGE,TABLE_NUM]      ## cho tất cả vào 1 list để truyền vào câu sql query
            print(VALUE_MASTER)

        
            #cur.execute(sql_master,VALUE_MASTER)       #********#          ## câu lệnh thực đẩy vào database bảng test_crl_huy_master, đẩy xong thì comment nó lại
            
            result=('write successful')
            print(result)
            
            ############# Đoạn này xử lý file pdf ###########

            df = camelot.read_pdf(URL,pages=f'{PAGE}')      ## dùng thư viện tách bảng truyền vào url và số thứ tự trang pdf cần tách
            
            print('########### '+BANK_NAME+' ##########')
            
            #display(df[table_num].df)

            df_pdf = df[TABLE_NUM].df                            ## truyền vào số thự tự bảng cần lấy data và lưu vào dataframe

            len_num = len(df_pdf.columns)                        ## Lấy số columns tron df để làm sạch loại bỏ các ký tự đặc biệt

            for i in range (len_num):
                df_pdf[i] = df_pdf[i].str.replace("\n","")
                df_pdf[i] = df_pdf[i].str.replace("\t","")
                df_pdf[i] = df_pdf[i].str.replace("(","")
                df_pdf[i] = df_pdf[i].str.replace(")","")
                df_pdf[i] = df_pdf[i].str.replace("%","")
                df_pdf[i] = df_pdf[i].str.replace("*","")
                df_pdf[i] = df_pdf[i].str.replace(",",".")
                df_pdf[i] = df_pdf[i].str.replace("  ","")
            
            df_pdf.insert(0,"Ma_NH",MA_NH)                         ## add thêm cột mã ngân hàng vào để phân biệt được giữa các bảng với nhau
            
            display(df_pdf)                                        ## hiển thị trên command line

            ######## đoạn này sẽ là ghi vào database sau khi đã có dữ liệu tù trên ######
            ## bảng trong database chứa sẵn 24 cột cứ add vào thôi


            ##### 1. Đoạn này là xử lý để tự tạo sql theo đúng số cột của bảng    
            add_num = len_num+2                                      ## số cột của df cần add + 1 là vì nãy add thêm cột mã ngân hàng

            str_column = 'COLUMN1'
            str_value = ':1'
            for i in range(2,add_num):                             ## chạy từ 2 đến số cột và cộng dồn vào 2 biến để truyền vào câu sql
                str_column = str_column+f',COLUMN{i}'
                str_value = str_value+f',:{i}'

            sql_bank_detail = f'INSERT INTO test_crl_huy_bank_detail ({str_column}) VALUES({str_value})'          ## được 1 câu sql hoàn chỉnh cho bảng đó

            #print(sql_bank_detail)
            ##### 2. đoạn này là sẽ ghi vào thực thi câu sql trên

            df_list = df_pdf.values.tolist()                       ## lấy ra từng row của df và chuyển thành lis, list này sẽ truyền vào giá trị VALUES
            n = 0                                       
            for i in df_pdf.iterrows():                     
                #cur.execute(sql_bank_detail,df_list[n])            #********# 
                n += 1
            

            ##### 3. Đoạn này là ghi data vào file execl

            # with pd.ExcelWriter("D:/HuyDq/CRL_BANK_RATE/excel/crl_pdf.xlsx", engine='openpyxl',mode='a') as writer: 
            #       df_pdf.to_excel(writer,encoding="utf-8"+u"",sheet_name=f'{BANK_NAME}')
        
        #write_log(check_point,check_compare,result,Err_sys,error)
    # except NameError as python_error:
    #     print("Bug", python_error)
    #     Err_sys = python_error

    #     connection.rollback()
            
    except cx_Oracle.DatabaseError as e: 
        print("Bug", e)
        Err_sys = e
        error = (f'Bank Error: {BANK_NAME}, type crawl: {TYPE}')
        print(error)
        check_compare='error'
        print("rollback")
        connection.rollback()
        write_log(check_point,check_compare,result,Err_sys,error)
    #connection.commit()  
    cur.close()
    connection.close()
    #########################
    
#### ghi data từ bank vào bảng master
def write_data_master_table():

    ### biến toàn cục
    # global check_compare            ## mặc định khi có lỗi là được gán bằng chuỗi 'error' khi truyền vào sp_log thì so sánh vs check_point không khớp thì ghi lỗi, khớp thì ko l
    # global error                    ## biến này khi xảy ra lỗi sẽ báo lỗi ở bank nào và thuộc kiểu crawl dạng nào
    # global result                   ## nếu thành công thì print "write successful"
    # global check_point              ## điểm check nếu có lỗi
    # global Err_sys                  ## ghi nhận lỗi hệ thống để show ra

    check_compare = ''
    error = ''
    result = ''
    Err_sys = ''
    result = '' 


    connection = cx_Oracle.connect('dwprod/oracle123@192.168.49.32:1521/uatdwh')    

    print("Database version:", connection.version)


    cur = connection.cursor()

    trun_master='truncate table test_crl_huy_master'
    trun_action='truncate table test_crl_huy_action'

    #cur.execute(trun_master)
    #cur.execute(trun_action)


    sql_master='INSERT INTO test_crl_huy_master (MA_NH,URL,TYPE,ATTRIBUTE,ACTION,PAGE,TABLE_NUM) VALUES(:1,:2,:3,:4,:5,:6,:7)'
    sql_action='INSERT INTO test_crl_huy_action (MA_NH,ATTRIBUTE_CLICK1,ATTRIBUTE_CLICK2,ATTRIBUTE_CLICK3) VALUES(:1,:2,:3,:4)'
    ### thuộc tính của bảng action

    #######
    leng =len(bank_web)
    check_point = 3                 ## đặt điểm check để khi có lỗi 
    try:
        for i in range(leng):
            

            MASTER=bank_web[i]
            MA_NH=MASTER[0]
            URL=MASTER[1]

            
            TYPE=MASTER[2]
            ATTRIBUTE=MASTER[3]
            ACTION=MASTER[4]
            BANK_NAME=MASTER[5]
            VALUE_MASTER=[MA_NH,URL,TYPE,ATTRIBUTE,ACTION,'','']
            print(VALUE_MASTER)                                     ### check xem có data chưa khi mà đã có dữ liệu thì đã có khóa r không thể excute được
            #cur.execute(sql_master,VALUE_MASTER)
            

            leng_action=len(MASTER)
            if(leng_action>6):

                #print(leng_action)
                #print(MASTER)
                #print(ACTION)
                MA_NH=MASTER[0]

                #print(MA_NH)
                ATTRIBUTE_CLICK1=MASTER[6]
                VALUE_ACTION=[MA_NH,ATTRIBUTE_CLICK1]
                try:
                    ATTRIBUTE_CLICK2=MASTER[7]
                    #print(ATTRIBUTE_CLICK2)
                    VALUE_ACTION.append(ATTRIBUTE_CLICK2)
                except:
                    ATTRIBUTE_CLICK2=''
                    VALUE_ACTION.append(ATTRIBUTE_CLICK2)
                try:
                    ATTRIBUTE_CLICK3=MASTER[8]
                    VALUE_ACTION.append(ATTRIBUTE_CLICK3)
                except:
                    ATTRIBUTE_CLICK3=''
                    VALUE_ACTION.append(ATTRIBUTE_CLICK3)

                print(VALUE_ACTION)                         ### check xem có data chưa khi mà đã có dữ liệu thì đã có khóa chính rồi không thể excute được

                #cur.execute(sql_action,VALUE_ACTION)

                result=('write successful')
                print(result)

        #write_log(check_point,check_compare,result,Err_sys,error)

    except cx_Oracle.DatabaseError as e: 
        print("Bug", e)
        Err_sys = e
        error = (f'Bank Error: {BANK_NAME}, type crawl: {TYPE}, action: {ACTION}')
        print(error)
        check_compare='error'
        #print(point)
        connection.rollback()
        write_log(check_point,check_compare,result,Err_sys,error)
    #connection.commit()
    cur.close()
    connection.close()
    #write_log(check_point,check_compare,result,Err_sys,error)

def main_web():
    ### biến toàn cục
    # global check_compare            ## mặc định khi có lỗi là được gán bằng chuỗi 'error' khi truyền vào sp_log thì so sánh vs check_point không khớp thì ghi lỗi, khớp thì ko l
    # global error                    ## biến này khi xảy ra lỗi sẽ báo lỗi ở bank nào và thuộc kiểu crawl dạng nào
    # global result                   ## nếu thành công thì print "write successful"
    # global check_point              ## điểm check nếu có lỗi
    # global Err_sys                  ## ghi nhận lỗi hệ thống để show ra

    check_compare = ''
    error = ''
    result = ''
    Err_sys = ''


    #### khai báo web ảo 

    path = "D:/Users/huydq4/Downloads/chromedriver_win32/chromedriver.exe"

    option = webdriver.ChromeOptions()
    option.add_argument("--headless")

    driver = webdriver.Chrome(executable_path=path, options=option)

    # tạo vòng for cho những list bank này chạy

    ### mở connection
    connection = cx_Oracle.connect('dwprod/oracle123@192.168.49.32:1521/uatdwh')  

    print("Database version:", connection.version)

    cur = connection.cursor()

    try:
        for i in range(len(bank_web)):
            check_point = 4
            # lấy trong list tổng ra mỗi bank để chạy
            # sẽ có những thuộc tính, đầu tiên là tên của bank để lưu làm tên sheet trong excel
            # link dẫn đến trang lãi suất của từng ngân hàng
            # xpath là đường dẫn để tìm đến đúng nội dung của table cần lấy
            
            # còn những thuộc tính sau đó là những cái xpath cần phải thao tác click để hiện
            each_bank = bank_web[i]                     ## lấy ra toàn bộ thông tin 1 ngân hàng
            ma_nh = each_bank[0]                    ## mã ngân hàng
            url_bank = each_bank[1]                 ## link ngân hàng


            # cái xpath này là xài chung ví dụ 1 table có cùng xpath những mỗi optine thì load 1 nội dung
            # nên sau khi thao tác xong thì ta phải lấy lại code html của xpath lần nữa
            type_bank = each_bank[2]
            xpath_bank = each_bank[3]
            action_bank = each_bank[4]
            # print(each_bank)
            bank_name = each_bank[5]

            # elem là toàn bộ code html có chứa nội dung cần lấy mà bao quát ở bên ngoài
            time.sleep(10)
            #driver.set_page_load_timeout(10)
            
            driver.get(f'{url_bank}')
            time.sleep(10)

            elem = driver.find_element_by_xpath(f'{xpath_bank}')

            leng=len(each_bank)

            # những each_bank nào vượt quá 6 thì cần phải thao tác click
            if(leng>6):
                # cho vòng for để thực hiện chuỗi thao tác        
                for i in range(6,leng):
                    option = each_bank[i]
                    loadMore =driver.find_element_by_xpath(xpath=f'{option}')  
                    loadMore.click()
                    time.sleep(2)

                # for xong thì lấy lại xpath lần nữa
                elem = driver.find_element_by_xpath(f'{xpath_bank}')

            #time.sleep(5)

            source_code = elem.get_attribute("outerHTML")
            soup = bs.BeautifulSoup(source_code,'lxml')

            table_parser=table_to_2d(soup.table)

            df=pd.DataFrame(table_parser)
            
            len_num=len(df.columns)
            for i in range (len_num):
                
                df[i] = df[i].str.replace("\n","")
                df[i] = df[i].str.replace("\t","")
                df[i] = df[i].str.replace("(","")
                df[i] = df[i].str.replace(")","")
                df[i] = df[i].str.replace("%","")
                df[i] = df[i].str.replace("*","")
                df[i] = df[i].str.replace(",",".")
                df[i] = df[i].str.replace("  ","")

            df.insert(0,"Ma_NH",ma_nh)

            print(f'################## {bank_name} ##############')
            display(df)

            #### insert thêm cột mã ngân hàng cho dễ phân biệt

            
            add_num=len(df.columns)+1

            str_column='COLUMN1'
            str_value=':1'
            for i in range(2,add_num):
                str_column=str_column+f',COLUMN{i}'
                str_value=str_value+f',:{i}'

            sql_bank_detail=f'INSERT INTO test_crl_huy_bank_detail ({str_column}) VALUES({str_value})'

            
            df_list = df.values.tolist()
            print(sql_bank_detail,df_list)
            n = 0
            for i in df.iterrows():
                
                cur.execute(sql_bank_detail,df_list[n])
                n += 1
                
            connection.commit()
            time.sleep(5)
        driver.close()

        write_log(check_point,check_compare,result,Err_sys,error)
        ### đóng connection 

            # ghi vào excel mỗi sheet là 1 bank
            # with pd.ExcelWriter("D:/HuyDq/CRL_BANK_RATE/excel/crl_selenium.xlsx", engine='openpyxl',mode='a') as writer: 
            #       df.to_excel(writer,encoding="utf-8"+u"",sheet_name=f'{name_bank}')


    except KeyboardInterrupt as interrupt:
        print("Bug python", interrupt)
        
        connection.rollback()
    except AttributeError as python_error:
        print("Bug python", python_error)
        Err_sys = str(python_error)
        Err_sys = Err_sys.replace("'","...")
        check_compare='error'
        error = (f'Bank Error: {bank_name}, type crawl: {type_bank}, action: {action_bank}')
        print(error)
        write_log(check_point,check_compare,result,Err_sys,error)
        connection.rollback()

    except NoSuchElementException as Xpath_error:
        print("Bug Xpath", Xpath_error)
        Err_sys = str(Xpath_error)
        check_compare='error'
        error = (f'Bank Error: {bank_name}, type crawl: {type_bank}, action: {action_bank}')
        print(error)
        write_log(check_point,check_compare,result,Err_sys,error)
        connection.rollback()
        
    except cx_Oracle.DatabaseError as e: 
        print("Bug Oralce", e)
        Err_sys = e
        error = (f'Bank Error: {bank_name}, type crawl: {type_bank}, action: {action_bank}')
        print(error)
        check_compare='error'
        #print(point)
        connection.rollback()
        driver.close()
        write_log(check_point,check_compare,result,Err_sys,error)
    # #connection.commit()  
    cur.close()
    connection.close()
    #########################

def write_log(check_point,check_compare,result,Err_sys,error):
    
    #print(point)
    #print(l_error)
    connection = cx_Oracle.connect('dwprod/oracle123@192.168.49.32:1521/uatdwh')  
    print("Database version:", connection.version)
    cur = connection.cursor()

    sql =f"""declare
    l_start_time varchar(20); --luu start time de update dung dong log ODI
    l_error varchar2(5000); --chi tiet loi
    l_num_record int;
    V_PREVIOUS_DATE date;
    p_holi_day date;
    l_today date;
    l_check_point number;
    l_check_temp number;
    l_check_compare nvarchar2(50);

    BEGIN
    
    l_check_point :=1;
    l_today := trunc(sysdate);
    l_start_time := OBI.PKG_ODI_LOG.FN_SET_ODI_START('82_1' --P_ORD
                                                    ,'GET_BANK_RAATE' --P_ODI_NAME
                                                    ,'GET_BANK_RAATE' --P_PROC_NAME
                                                    ,l_today --P_ODI_DATE
    /*BEGIN PYTHON */                                               );
    l_check_point :={check_point};
    l_check_compare:='{check_compare}';
    l_check_temp:=l_check_compare;                                            
    DBMS_OUTPUT.put_line('{result}'); 
    

    --GHI LOG LOI HE THONG
       
    /*END PYTHON*/
        OBI.PKG_ODI_LOG.SP_SET_ODI_END('82_1' --P_ORD
                                ,'GET_BANK_RAATE' --P_ODI_NAME
                                ,'GET_BANK_RAATE' --P_PROC_NAME
                                ,l_today --P_ODI_DATE
                                ,l_start_time --P_START_TIME
                                );
    EXCEPTION
    when others then
        l_error := '{Err_sys}'||' At point :'||l_check_point || ' and {error}' ; --Lay loi he thong
        DBMS_OUTPUT.put_line(l_error);
            OBI.PKG_ODI_LOG.SP_SET_ODI_ERROR('82_1' --P_ORD
                                ,'GET_BANK_RAATE' --P_ODI_NAME
                                ,'GET_BANK_RAATE' --P_PROC_NAME
                                ,l_today --P_ODI_DATE
                                ,l_start_time --P_START_TIME
                                ,l_error --P_DESC
                                );                          
    end;  """
    cur.execute(sql)

### hàm parser theo đúng hiển thị table ở trên web chia cột rõ ràng  -- hàm này copy trên mạng về truyền cho nó cái html của table đó là được
def table_to_2d(table_tag):
    rowspans = []  # track pending rowspans
    rows = table_tag.find_all('tr')

    # first scan, see how many columns we need
    colcount = 0
    for r, row in enumerate(rows):
        cells = row.find_all(['td', 'th'], recursive=False)
        # count columns (including spanned).
        # add active rowspans from preceding rows
        # we *ignore* the colspan value on the last cell, to prevent
        # creating 'phantom' columns with no actual cells, only extended
        # colspans. This is achieved by hardcoding the last cell width as 1. 
        # a colspan of 0 means “fill until the end” but can really only apply
        # to the last cell; ignore it elsewhere. 
        colcount = max(
            colcount,
            sum(int(c.get('colspan', 1)) or 1 for c in cells[:-1]) + len(cells[-1:]) + len(rowspans))
        # update rowspan bookkeeping; 0 is a span to the bottom. 
        rowspans += [int(c.get('rowspan', 1)) or len(rows) - r for c in cells]
        rowspans = [s - 1 for s in rowspans if s > 1]

    # it doesn't matter if there are still rowspan numbers 'active'; no extra
    # rows to show in the table means the larger than 1 rowspan numbers in the
    # last table row are ignored.

    # build an empty matrix for all possible cells
    table = [[None] * colcount for row in rows]

    # fill matrix from row data
    rowspans = {}  # track pending rowspans, column number mapping to count
    for row, row_elem in enumerate(rows):
        span_offset = 0  # how many columns are skipped due to row and colspans 
        for col, cell in enumerate(row_elem.find_all(['td', 'th'], recursive=False)):
            # adjust for preceding row and colspans
            col += span_offset
            while rowspans.get(col, 0):
                span_offset += 1
                col += 1

            # fill table data
            rowspan = rowspans[col] = int(cell.get('rowspan', 1)) or len(rows) - row
            colspan = int(cell.get('colspan', 1)) or colcount - col
            # next column is offset by the colspan
            span_offset += colspan - 1
            value = cell.get_text()
            for drow, dcol in product(range(rowspan), range(colspan)):
                try:
                    table[row + drow][col + dcol] = value
                    rowspans[col + dcol] = rowspan
                except IndexError:
                    # rowspan or colspan outside the confines of the table
                    pass

        # update rowspan bookkeeping
        rowspans = {c: s - 1 for c, s in rowspans.items() if s > 1}

    return table

if __name__ == "__main__":
    
    download_file_pdf()
    main_pdf()
    write_data_master_table()
    main_web()
    #write_log()


