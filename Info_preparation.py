# CHUẨN BỊ INFO PDF, WEB VÀ DOWNLOAD CÁC FILE PDF
import urllib.request
from datetime import date
import os
from itertools import count, product
from http.client import IncompleteRead

def Pdf_info():
    global Bank_pdf
    Bank_pdf = []
    File = open("pdf_info.txt", "r", encoding="utf8")
    Content = File.read()
    Content_list = Content.split("\n")
    File.close()

    list_index = []
    temp = []
    leng_content = len(Content_list)
    for i in range(leng_content):
        if Content_list[i] == '':
            list_index.append(i)
    leng_index = len(list_index)
    n = 0
    for i in range(leng_index):
        for j in range(n, list_index[i]):
            a = Content_list[j].replace(" ", "")
            temp.append(a)
        Bank_pdf.append(temp)
        temp = []
        n = list_index[i]+1


def Web_info():
    global Bank_web
    Bank_web = []
    File = open("web_info.txt", "r", encoding="utf8")
    Content = File.read()
    Content_list = Content.split("\n")
    File.close()
    list_index = []
    temp = []
    leng_content = len(Content_list)
    for i in range(leng_content):
        if Content_list[i] == '':
            list_index.append(i)

    leng_index = len(list_index)
    n = 0
    for i in range(leng_index):
        for j in range(n, list_index[i]):
            a = Content_list[j].replace(" ", "")
            temp.append(a)
        Bank_web.append(temp)
        temp = []
        n = list_index[i]+1


def download_file_pdf():
    opener = urllib.request.URLopener()
    opener.addheader('User-Agent', 'whatever')

    today = date.today()
    pdf_date = today.strftime("%d/%m/%Y")
    pdf_date = str(pdf_date).replace("/", "_")
    global listt
    listt = []
    leng = len(Bank_pdf)  # lấy dộ dài của cả danh sách bank

    try:
        for i in range(leng):  # chạy vòng for
            MASTER = Bank_pdf[i]  # lấy ra từng bank để thực thi
            MA_NH = MASTER[0]
            URL = MASTER[1]
            TYPE = MASTER[2]
            NAME = MASTER[5]
            PATH_DESC = F'pdf/{NAME}_{pdf_date}.pdf'
            listt.append(PATH_DESC)
            print(PATH_DESC)
            filename, headers = opener.retrieve(f'{URL}', f'{PATH_DESC}')

            # urllib.request.urlretrieve(f'{URL}',f'{PATH_DESC}')
    except IncompleteRead:
        if len(listt) > 0:
            for i in range(len(listt)):
                os.remove(f'{listt[i]}')
            listt = []
            for i in range(leng):  # chạy vòng for
                MASTER = Bank_pdf[i]  # lấy ra từng bank để thực thi
                MA_NH = MASTER[0]
                URL = MASTER[1]
                TYPE = MASTER[2]
                NAME = MASTER[5]
                PATH_DESC = F'pdf/{NAME}_{pdf_date}.pdf'
                listt.append(PATH_DESC)
                # print(PATH_DESC)
                filename, headers = opener.retrieve(f'{URL}', f'{PATH_DESC}')

                # urllib.request.urlretrieve(f'{URL}',f'{PATH_DESC}')
                continue
        else:

            for i in range(leng):  # chạy vòng for
                MASTER = Bank_pdf[i]  # lấy ra từng bank để thực thi
                MA_NH = MASTER[0]
                URL = MASTER[1]
                TYPE = MASTER[2]
                NAME = MASTER[5]
                PATH_DESC = F'pdf/{NAME}_{pdf_date}.pdf'
                listt.append(PATH_DESC)
                # print(PATH_DESC)
                filename, headers = opener.retrieve(f'{URL}', f'{PATH_DESC}')

                # urllib.request.urlretrieve(f'{URL}',f'{PATH_DESC}')
                continue
    except urllib.error.URLError as a:
        print(a)

pdf_ma_nh = []
pdf_PATH = []
pdf_PAGE_NUM = []
pdf_TABLE_NUM = []
pdf_BANK_NAME = []
pdf_TYPE = []


def lists_pdf():

    leng_pdf = range(len(Bank_pdf))
    for i in leng_pdf:

        MASTER = Bank_pdf[i]
        MA_NH = MASTER[0]
        PATH = MASTER[1]
        TYPE = MASTER[2]
        PAGE_NUM = MASTER[3]
        TABLE_NUM = MASTER[4]
        BANK_NAME = MASTER[5]
        pdf_ma_nh.append(MA_NH)
        pdf_PATH.append(PATH)
        pdf_TYPE.append(TYPE)
        pdf_PAGE_NUM.append(PAGE_NUM)
        pdf_TABLE_NUM.append(TABLE_NUM)
        pdf_BANK_NAME.append(BANK_NAME)


global listt
listt=[]
def download_pdf(i):
    opener = urllib.request.URLopener()
    opener.addheader('User-Agent', 'whatever')
    global listt
    today = date.today()
    pdf_date = today.strftime("%d/%m/%Y")
    pdf_date = str(pdf_date).replace("/", "_")

    MASTER = Bank_pdf[i]  # lấy ra từng bank để thực thi
    MA_NH = MASTER[0]
    URL = MASTER[1]
    TYPE = MASTER[2]
    NAME = MASTER[5]
    PATH_DESC = F'pdf/{NAME}_{pdf_date}.pdf'
    listt.append(PATH_DESC)
    print(PATH_DESC)
    filename, headers = opener.retrieve(f'{URL}', f'{PATH_DESC}')



