
from camelot.io import read_pdf
import numpy as np
import ssl


from logging import exception
from sys import excepthook
from selenium import webdriver
import urllib3
import re
import time
import pandas as pd
import bs4 as bs
from collections import defaultdict
from selenium.webdriver.chrome.options import Options
from itertools import product
from selenium.common.exceptions import NoSuchElementException
from os import name
from sys import path
import urllib.request
from datetime import date
from http.client import IncompleteRead
import os
from datetime import datetime

##### Một vài lỗi thường gặp
### 1. ORA-00001: unique constraint : là trùng khóa chính tức là đã có dữ liệu trong bảng có khóa chính cần truncate hoặc dùng update hoặc tắt lệnh cur.execute
### 2. Message: no such element: Unable to locate element : là không tìm ra element trên web vì web load chưa được cần tăng time.sleep lên vài chục giây hoặc trang web cập nhật mới cần tìm lại
### 3. Một số lỗi khác dư dấu .,()"@$%#^"... thì cần check lại nó sẽ báo là NOT DEFINE 


#### ----- Note ------- #####
#### 1. có 1 # là comment mở ra để chạy câu lệnh đó
#### 2. có 2 ## là comment chú thích
#### 3. có nhiều ### là 1 khu vực cũng để chú thích
#### 4. câu thực thi #********# để ghi vào database

def main_pdf():
    ########################### KHU VUC LOAD DATA VAO DB ######################
    
    # global check_compare            ## mặc định khi có lỗi là được gán bằng chuỗi 'error' khi truyền vào sp_log thì so sánh vs check_point không khớp thì ghi lỗi, khớp thì ko l
    # global error                    ## biến này khi xảy ra lỗi sẽ báo lỗi ở bank nào và thuộc kiểu crawl dạng nào
    # global result                   ## nếu thành công thì print "write successful"
    # global check_point              ## điểm check nếu có lỗi
    # global Err_sys                  ## ghi nhận lỗi hệ thống để show ra

    check_compare = ''
    error = ''
    result = ''
    Err_sys = ''
    result = '' 
    connection = cx_Oracle.connect('dwprod/oracle123@192.168.49.32:1521/uatdwh')  
    #print("Database version:", connection.version)
    cur = connection.cursor()

    del_detail_pdf="""delete from TEST_CRL_HUY_BANK_DETAIL
                where column1 in (
                                select distinct (column1) from TEST_CRL_HUY_MASTER m, TEST_CRL_HUY_BANK_DETAIL d
                                where m.ma_nh=d.column1 and m.type=N'PDF')"""
    del_master="""delete from TEST_CRL_HUY_MASTER m where m.type=N'PDF'"""

    commit_sql="commit"


    cur.execute(del_detail_pdf)
    cur.execute(del_master)
    cur.execute(commit_sql)

    ### Tạo sẵn câu sql insert các thuộc tính vào bảng -> sau này sẽ xét thay đổi cập nhật chứ không phải insert 
    sql_master='INSERT INTO test_crl_huy_master (MA_NH,URL,TYPE,ATTRIBUTE,ACTION,PAGE,TABLE_NUM) VALUES(:1,:2,:3,:4,:5,:6,:7)'



    ####### Khu vực xử lý data

    leng =len(bank_pdf)                     ## lấy dộ dài của cả danh sách bank
    
    try:
        for i in range(leng):               ## chạy vòng for
            check_point = 2

            MASTER = bank_pdf[i]                  ## lấy ra từng bank để thực thi          
            MA_NH = MASTER[0]                 
            URL = listt[i]                   
            TYPE = MASTER[2]
            PAGE = MASTER[3]
            TABLE_NUM = int(MASTER[4])
            BANK_NAME = MASTER[5]

            VALUE_MASTER = [MA_NH,URL,TYPE,'','',PAGE,TABLE_NUM]      ## cho tất cả vào 1 list để truyền vào câu sql query
            #print(VALUE_MASTER)

        
            cur.execute(sql_master,VALUE_MASTER)       #********#          ## câu lệnh thực đẩy vào database bảng test_crl_huy_master, đẩy xong thì comment nó lại
            
            result=('write successful')
            #print(result)
            
            ############# Đoạn này xử lý file pdf ###########

            df = camelot.read_pdf(URL,pages=f'{PAGE}')      ## dùng thư viện tách bảng truyền vào url và số thứ tự trang pdf cần tách
            
            #print('########### '+BANK_NAME+' ##########')
            
            #display(df[table_num].df)

            df_pdf = df[TABLE_NUM].df                            ## truyền vào số thự tự bảng cần lấy data và lưu vào dataframe

            len_num = len(df_pdf.columns)                        ## Lấy số columns tron df để làm sạch loại bỏ các ký tự đặc biệt

            for i in range (len_num):
                df_pdf[i] = df_pdf[i].str.replace("\n","")
                df_pdf[i] = df_pdf[i].str.replace("\t","")
                df_pdf[i] = df_pdf[i].str.replace("(","")
                df_pdf[i] = df_pdf[i].str.replace(")","")
                df_pdf[i] = df_pdf[i].str.replace("%","")
                df_pdf[i] = df_pdf[i].str.replace("*","")
                df_pdf[i] = df_pdf[i].str.replace(",",".")
                df_pdf[i] = df_pdf[i].str.replace("  ","")
            
            df_pdf.insert(0,"Ma_NH",MA_NH)                         ## add thêm cột mã ngân hàng vào để phân biệt được giữa các bảng với nhau
            
            #display(df_pdf)                                        ## hiển thị trên command line

            ######## đoạn này sẽ là ghi vào database sau khi đã có dữ liệu tù trên ######
            ## bảng trong database chứa sẵn 24 cột cứ add vào thôi


            ##### 1. Đoạn này là xử lý để tự tạo sql theo đúng số cột của bảng    
            add_num = len_num+2                                      ## số cột của df cần add + 1 là vì nãy add thêm cột mã ngân hàng

            str_column = 'COLUMN1'
            str_value = ':1'
            for i in range(2,add_num):                             ## chạy từ 2 đến số cột và cộng dồn vào 2 biến để truyền vào câu sql
                str_column = str_column+f',COLUMN{i}'
                str_value = str_value+f',:{i}'

            sql_bank_detail = f'INSERT INTO test_crl_huy_bank_detail ({str_column}) VALUES({str_value})'          ## được 1 câu sql hoàn chỉnh cho bảng đó

            #print(sql_bank_detail)
            ##### 2. đoạn này là sẽ ghi vào thực thi câu sql trên

            df_list = df_pdf.values.tolist()                       ## lấy ra từng row của df và chuyển thành lis, list này sẽ truyền vào giá trị VALUES
            n = 0                                       
            for i in df_pdf.iterrows():                     
                cur.execute(sql_bank_detail,df_list[n])            #********# 
                n += 1
            

            ##### 3. Đoạn này là ghi data vào file execl

            # with pd.ExcelWriter("D:/HuyDq/CRL_BANK_RATE/excel/crl_pdf.xlsx", engine='openpyxl',mode='a') as writer: 
            #       df_pdf.to_excel(writer,encoding="utf-8"+u"",sheet_name=f'{BANK_NAME}')
        
        #write_log(check_point,check_compare,result,Err_sys,error)
    # except NameError as python_error:
    #     print("Bug", python_error)
    #     Err_sys = python_error

    #     connection.rollback()
            
    except cx_Oracle.DatabaseError as e: 
        print("Bug", e)
        Err_sys = e
        error = (f'Bank Error: {BANK_NAME}, type crawl: {TYPE}')
        print(error)
        check_compare='error'
        print("rollback")
        connection.rollback()
        write_log(check_point,check_compare,result,Err_sys,error)
    connection.commit()  
    cur.close()
    connection.close()
    #########################
    
#### ghi data từ bank vào bảng master
def write_data_master_table():

    ### biến toàn cục
    # global check_compare            ## mặc định khi có lỗi là được gán bằng chuỗi 'error' khi truyền vào sp_log thì so sánh vs check_point không khớp thì ghi lỗi, khớp thì ko l
    # global error                    ## biến này khi xảy ra lỗi sẽ báo lỗi ở bank nào và thuộc kiểu crawl dạng nào
    # global result                   ## nếu thành công thì print "write successful"
    # global check_point              ## điểm check nếu có lỗi
    # global Err_sys                  ## ghi nhận lỗi hệ thống để show ra

    check_compare = ''
    error = ''
    result = ''
    Err_sys = ''
    result = '' 


    connection = cx_Oracle.connect('dwprod/oracle123@192.168.49.32:1521/uatdwh')    

    print("Database version:", connection.version)


    cur = connection.cursor()

    del_master="""delete from TEST_CRL_HUY_MASTER m where m.type=N'Web'"""
    trun_action='''truncate table test_crl_huy_action'''

    cur.execute(del_master)
    cur.execute(trun_action)


    sql_master='INSERT INTO test_crl_huy_master (MA_NH,URL,TYPE,ATTRIBUTE,ACTION,PAGE,TABLE_NUM) VALUES(:1,:2,:3,:4,:5,:6,:7)'
    sql_action='INSERT INTO test_crl_huy_action (MA_NH,ATTRIBUTE_CLICK1,ATTRIBUTE_CLICK2,ATTRIBUTE_CLICK3) VALUES(:1,:2,:3,:4)'
    ### thuộc tính của bảng action

    #######
    leng =len(bank_web)
    check_point = 3                 ## đặt điểm check để khi có lỗi 
    try:
        for i in range(leng):
            

            MASTER=bank_web[i]
            MA_NH=MASTER[0]
            URL=MASTER[1]

            
            TYPE=MASTER[2]
            ATTRIBUTE=MASTER[3]
            ACTION=MASTER[4]
            BANK_NAME=MASTER[5]
            VALUE_MASTER=[MA_NH,URL,TYPE,ATTRIBUTE,ACTION,'','']
            #print(VALUE_MASTER)                                     ### check xem có data chưa khi mà đã có dữ liệu thì đã có khóa r không thể excute được
            cur.execute(sql_master,VALUE_MASTER)
            

            leng_action=len(MASTER)
            if(leng_action>6):

                #print(leng_action)
                #print(MASTER)
                #print(ACTION)
                MA_NH=MASTER[0]

                #print(MA_NH)
                ATTRIBUTE_CLICK1=MASTER[6]
                VALUE_ACTION=[MA_NH,ATTRIBUTE_CLICK1]
                try:
                    ATTRIBUTE_CLICK2=MASTER[7]
                    #print(ATTRIBUTE_CLICK2)
                    VALUE_ACTION.append(ATTRIBUTE_CLICK2)
                except:
                    ATTRIBUTE_CLICK2=''
                    VALUE_ACTION.append(ATTRIBUTE_CLICK2)
                try:
                    ATTRIBUTE_CLICK3=MASTER[8]
                    VALUE_ACTION.append(ATTRIBUTE_CLICK3)
                except:
                    ATTRIBUTE_CLICK3=''
                    VALUE_ACTION.append(ATTRIBUTE_CLICK3)

                #print(VALUE_ACTION)                         ### check xem có data chưa khi mà đã có dữ liệu thì đã có khóa chính rồi không thể excute được

                cur.execute(sql_action,VALUE_ACTION)

                result=('write successful')
                #print(result)

        #write_log(check_point,check_compare,result,Err_sys,error)

    except cx_Oracle.DatabaseError as e: 
        print("Bug", e)
        Err_sys = e
        error = (f'Bank Error: {BANK_NAME}, type crawl: {TYPE}, action: {ACTION}')
        print(error)
        check_compare='error'
        #print(point)
        connection.rollback()
        write_log(check_point,check_compare,result,Err_sys,error)
    connection.commit()
    cur.close()
    connection.close()
    #write_log(check_point,check_compare,result,Err_sys,error)

def main_web():
    ### biến toàn cục
    # global check_compare            ## mặc định khi có lỗi là được gán bằng chuỗi 'error' khi truyền vào sp_log thì so sánh vs check_point không khớp thì ghi lỗi, khớp thì ko l
    # global error                    ## biến này khi xảy ra lỗi sẽ báo lỗi ở bank nào và thuộc kiểu crawl dạng nào
    # global result                   ## nếu thành công thì print "write successful"
    # global check_point              ## điểm check nếu có lỗi
    # global Err_sys                  ## ghi nhận lỗi hệ thống để show ra

    check_compare = ''
    error = ''
    result = ''
    Err_sys = ''

    #### khai báo web ảo 

    path = "chromedriver.exe"

    option = webdriver.ChromeOptions()

    option.add_argument("--headless")
    prefs = {"profile.managed_default_content_settings.images": 2}
    option.add_experimental_option("prefs", prefs)

    driver = webdriver.Chrome(executable_path=path, options=option)

    # tạo vòng for cho những list bank này chạy

    ### mở connection
    connection = cx_Oracle.connect('dwprod/oracle123@192.168.49.32:1521/uatdwh')  

    #print("Database version:", connection.version)

    cur = connection.cursor()
   
    del_detail_web="""delete from TEST_CRL_HUY_BANK_DETAIL
                        where column1 in (
                                        select distinct (column1) from TEST_CRL_HUY_MASTER m, TEST_CRL_HUY_BANK_DETAIL d
                                        where m.ma_nh=d.column1 and m.type=N'Web')"""
    cur.execute(del_detail_web)
    
    try:
        for i in range(len(bank_web)):
            check_point = 4
            # lấy trong list tổng ra mỗi bank để chạy
            # sẽ có những thuộc tính, đầu tiên là tên của bank để lưu làm tên sheet trong excel
            # link dẫn đến trang lãi suất của từng ngân hàng
            # xpath là đường dẫn để tìm đến đúng nội dung của table cần lấy
            
            # còn những thuộc tính sau đó là những cái xpath cần phải thao tác click để hiện
            each_bank = bank_web[i]                     ## lấy ra toàn bộ thông tin 1 ngân hàng
            ma_nh = each_bank[0]                    ## mã ngân hàng
            url_bank = each_bank[1]                 ## link ngân hàng


            # cái xpath này là xài chung ví dụ 1 table có cùng xpath những mỗi optine thì load 1 nội dung
            # nên sau khi thao tác xong thì ta phải lấy lại code html của xpath lần nữa
            type_bank = each_bank[2]
            xpath_bank = each_bank[3]
            action_bank = each_bank[4]
            # print(each_bank)
            bank_name = each_bank[5]

            # elem là toàn bộ code html có chứa nội dung cần lấy mà bao quát ở bên ngoài
            time.sleep(5)
            #driver.set_page_load_timeout(10)
            
            driver.get(f'{url_bank}')
            time.sleep(3)

            elem = driver.find_element_by_xpath(f'{xpath_bank}')

            leng=len(each_bank)

            # những each_bank nào vượt quá 6 thì cần phải thao tác click
            if(leng>6):
                # cho vòng for để thực hiện chuỗi thao tác        
                for i in range(6,leng):
                    option = each_bank[i]
                    loadMore =driver.find_element_by_xpath(xpath=f'{option}')  
                    loadMore.click()
                    time.sleep(1)

                # for xong thì lấy lại xpath lần nữa
                elem = driver.find_element_by_xpath(f'{xpath_bank}')

            source_code = elem.get_attribute("outerHTML")
            soup = bs.BeautifulSoup(source_code,'lxml')

            table_parser=table_to_2d(soup.table)

            df=pd.DataFrame(table_parser)
            
            len_num=len(df.columns)
            for i in range (len_num):
                
                df[i] = df[i].str.replace("\n","")
                df[i] = df[i].str.replace("\t","")
                df[i] = df[i].str.replace("(","")
                df[i] = df[i].str.replace(")","")
                df[i] = df[i].str.replace("%","")
                df[i] = df[i].str.replace("*","")
                df[i] = df[i].str.replace(",",".")
                df[i] = df[i].str.replace("  ","")

            df.insert(0,"Ma_NH",ma_nh)

            #print(f'################## {bank_name} ##############')
            #display(df)

            #### insert thêm cột mã ngân hàng cho dễ phân biệt

            
            add_num=len(df.columns)+1

            str_column='COLUMN1'
            str_value=':1'
            for i in range(2,add_num):
                str_column=str_column+f',COLUMN{i}'
                str_value=str_value+f',:{i}'

            sql_bank_detail=f'INSERT INTO test_crl_huy_bank_detail ({str_column}) VALUES({str_value})'

            
            df_list = df.values.tolist()
            #print(sql_bank_detail,df_list)
            n = 0
            for i in df.iterrows():
                
                cur.execute(sql_bank_detail,df_list[n])
                n += 1
                
            connection.commit()
            time.sleep(2)
        driver.close()

        write_log(check_point,check_compare,result,Err_sys,error)
        ### đóng connection 

            # ghi vào excel mỗi sheet là 1 bank
        # with pd.ExcelWriter("CLEAN/crl_selenium.xlsx", engine='openpyxl',mode='a') as writer: 
        #         df.to_excel(writer,encoding="utf-8"+u"",sheet_name=f'{bank_name}')


    except KeyboardInterrupt as interrupt:
        print("Bug python", interrupt)
        
        connection.rollback()
    except AttributeError as python_error:
        print("Bug python", python_error)
        Err_sys = str(python_error)
        Err_sys = Err_sys.replace("'","...")
        check_compare='error'
        error = (f'Bank Error: {bank_name}, type crawl: {type_bank}, action: {action_bank}')
        print(error)
        write_log(check_point,check_compare,result,Err_sys,error)
        connection.rollback()

    except NoSuchElementException as Xpath_error:
        print("Bug Xpath", Xpath_error)
        Err_sys = str(Xpath_error)
        check_compare='error'
        error = (f'Bank Error: {bank_name}, type crawl: {type_bank}, action: {action_bank}')
        print(error)
        write_log(check_point,check_compare,result,Err_sys,error)
        connection.rollback()
        
    except cx_Oracle.DatabaseError as e: 
        print("Bug Oralce", e)
        Err_sys = e
        error = (f'Bank Error: {bank_name}, type crawl: {type_bank}, action: {action_bank}')
        print(error)
        check_compare='error'
        #print(point)
        connection.rollback()
        driver.close()
        write_log(check_point,check_compare,result,Err_sys,error)
    connection.commit()  
    cur.close()
    connection.close()
    #########################


### hàm parser theo đúng hiển thị table ở trên web chia cột rõ ràng  


if __name__ == "__main__":
    now = datetime.now()
    print("now =", now)

    # dd/mm/YY H:M:S
    dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
    read_pdf_info_txt()
    read_web_info_txt()
    download_file_pdf()
    # main_pdf()
    # write_data_master_table()
    # main_web()
    now1 = datetime.now()
    dt_string1 = now1.strftime("%d/%m/%Y %H:%M:%S")
    print(f'start time: {dt_string}')
    print(f'end time: {dt_string1}')
    #write_log()
    #print('je')


