from http.client import GONE
import multiprocessing
from joblib import Parallel, delayed
from IPython.display import display
import camelot
import Info_preparation
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from sys import path
import time
from itertools import count, product
import bs4 as bs
import pandas as pd
import cx_Oracle
import Log_dbOracle
from selenium.common.exceptions import NoSuchElementException
import clean.clean

# Create driver object. Opens browser window. selenium
path = "chromedriver.exe"
option = webdriver.ChromeOptions()
option.add_argument("--headless")
prefs = {"profile.managed_default_content_settings.images": 2}
option.add_experimental_option("prefs", prefs)

# Start Code Preparation
Info_preparation.Pdf_info()
Info_preparation.Web_info()

# print(Info_preparation.web_ATTRIBUTE)
bank_pdf = Info_preparation.Bank_pdf
bank_web = Info_preparation.Bank_web


def table_to_2d(table_tag):
    rowspans = []  # track pending rowspans
    rows = table_tag.find_all('tr')

    # first scan, see how many columns we need
    colcount = 0
    for r, row in enumerate(rows):
        cells = row.find_all(['td', 'th'], recursive=False)
        # count columns (including spanned).
        # add active rowspans from preceding rows
        # we *ignore* the colspan value on the last cell, to prevent
        # creating 'phantom' columns with no actual cells, only extended
        # colspans. This is achieved by hardcoding the last cell width as 1.
        # a colspan of 0 means “fill until the end” but can really only apply
        # to the last cell; ignore it elsewhere.
        colcount = max(
            colcount,
            sum(int(c.get('colspan', 1)) or 1 for c in cells[:-1]) + len(cells[-1:]) + len(rowspans))
        # update rowspan bookkeeping; 0 is a span to the bottom.
        rowspans += [int(c.get('rowspan', 1)) or len(rows) - r for c in cells]
        rowspans = [s - 1 for s in rowspans if s > 1]

    # it doesn't matter if there are still rowspan numbers 'active'; no extra
    # rows to show in the table means the larger than 1 rowspan numbers in the
    # last table row are ignored.

    # build an empty matrix for all possible cells
    table = [[None] * colcount for row in rows]

    # fill matrix from row data
    rowspans = {}  # track pending rowspans, column number mapping to count
    for row, row_elem in enumerate(rows):
        span_offset = 0  # how many columns are skipped due to row and colspans
        for col, cell in enumerate(row_elem.find_all(['td', 'th'], recursive=False)):
            # adjust for preceding row and colspans
            col += span_offset
            while rowspans.get(col, 0):
                span_offset += 1
                col += 1

            # fill table data
            rowspan = rowspans[col] = int(
                cell.get('rowspan', 1)) or len(rows) - row
            colspan = int(cell.get('colspan', 1)) or colcount - col
            # next column is offset by the colspan
            span_offset += colspan - 1
            value = cell.get_text()
            for drow, dcol in product(range(rowspan), range(colspan)):
                try:
                    table[row + drow][col + dcol] = value
                    rowspans[col + dcol] = rowspan
                except IndexError:
                    # rowspan or colspan outside the confines of the table
                    pass

        # update rowspan bookkeeping
        rowspans = {c: s - 1 for c, s in rowspans.items() if s > 1}

    return table


def paser_pdf(ma_nh, path, page_num, table_num, name):
    # global check_compare            ## mặc định khi có lỗi là được gán bằng chuỗi 'error' khi truyền vào sp_log thì so sánh vs check_point không khớp thì ghi lỗi, khớp thì ko l
    # global error                    ## biến này khi xảy ra lỗi sẽ báo lỗi ở bank nào và thuộc kiểu crawl dạng nào
    # global result                   ## nếu thành công thì print "write successful"
    # global check_point              ## điểm check nếu có lỗi
    # global Err_sys                  ## ghi nhận lỗi hệ thống để show ra
    check_compare = ''
    error = ''
    result = ''
    Err_sys = ''
    result = ''
    # Khu vực xử lý data

    MA_NH = ma_nh
    PATH = path
    PAGE_NUM = page_num
    TABLE_NUM = int(table_num)
    BANK_NAME = name

    df = camelot.read_pdf(PATH, pages=f'{PAGE_NUM}')

    df_pdf = df[TABLE_NUM].df

    len_num = len(df_pdf.columns)

    for i in range(len_num):
        df_pdf[i] = df_pdf[i].str.replace("\n", "")
        df_pdf[i] = df_pdf[i].str.replace("\t", "")
        df_pdf[i] = df_pdf[i].str.replace("(", "")
        df_pdf[i] = df_pdf[i].str.replace(")", "")
        df_pdf[i] = df_pdf[i].str.replace("%", "")
        df_pdf[i] = df_pdf[i].str.replace("*", "")
        df_pdf[i] = df_pdf[i].str.replace(",", ".")
        df_pdf[i] = df_pdf[i].str.replace("  ", "")

    print(BANK_NAME)
    df_pdf.insert(0, "Ma_NH", MA_NH)
    display(df_pdf)

    # 3. Đoạn này là ghi data vào file execl

    with pd.ExcelWriter("clean/crl_selenium.xlsx", engine='openpyxl', mode='a', if_sheet_exists='replace') as writer:
        df_pdf.to_excel(writer, encoding="utf-8"+u"",sheet_name=f'{BANK_NAME}')


global countt
lengg = len(bank_web)
countt = lengg

# def test():
#     global countt
#     countt=countt-1


def paser_html(i):
    # biến toàn cục
    # global check_compare            ## mặc định khi có lỗi là được gán bằng chuỗi 'error' khi truyền vào sp_log thì so sánh vs check_point không khớp thì ghi lỗi, khớp thì ko l
    # global error                    ## biến này khi xảy ra lỗi sẽ báo lỗi ở bank nào và thuộc kiểu crawl dạng nào
    # global result                   ## nếu thành công thì print "write successful"
    # global check_point              ## điểm check nếu có lỗi
    # global Err_sys                  ## ghi nhận lỗi hệ thống để show ra
    global countt
    check_compare = ''
    error = ''
    result = ''
    Err_sys = ''

    Bank_web = bank_web[i]
    ma_nh = Bank_web[0]  # mã ngân hàng
    ma_nh1 = str(ma_nh)
    url_bank = Bank_web[1]  # link ngân hàng
    type_bank = Bank_web[2]
    xpath_bank = Bank_web[3]
    action_bank = Bank_web[4]
    bank_name = Bank_web[5]
    check_point = 4
    global countt  # check xem có hoàn thành hết 20 web không

    try:
        # mở connection
        connection = cx_Oracle.connect(
            'dwprod/oracle123@192.168.49.32:1521/uatdwh')

        #print("Database version:", connection.version)

        cur = connection.cursor()

        del_detail_web = f"""delete from TEST_CRL_HUY_MASTER m where m.ma_nh=N'{ma_nh1}'"""
        cur.execute(del_detail_web)
        connection.commit()
        driver = webdriver.Chrome(executable_path=path, options=option)
        driver.get(f"{url_bank}")
        time.sleep(10)
        elem = driver.find_element_by_xpath(f'{xpath_bank}')
        leng = len(Bank_web)
        if(leng > 6):
            # cho vòng for để thực hiện chuỗi thao tác
            for i in range(6, leng):
                options = Bank_web[i]
                loadMore = driver.find_element_by_xpath(xpath=f'{options}')
                loadMore.click()
                time.sleep(1)

            # for xong thì lấy lại xpath lần nữa
            elem = driver.find_element_by_xpath(f'{xpath_bank}')

        source_code = elem.get_attribute("outerHTML")
        soup = bs.BeautifulSoup(source_code, 'lxml')

        table_parser = table_to_2d(soup.table)

        df = pd.DataFrame(table_parser)

        len_num = len(df.columns)
        for i in range(len_num):

            df[i] = df[i].str.replace("\n", "", regex=True)
            df[i] = df[i].str.replace("\t", "", regex=True)
            df[i] = df[i].str.replace("(", "", regex=True)
            df[i] = df[i].str.replace(")", "", regex=True)
            df[i] = df[i].str.replace("%", "", regex=True)
            df[i] = df[i].str.replace("*", "", regex=True)
            df[i] = df[i].str.replace(",", ".", regex=True)
            df[i] = df[i].str.replace("  ", "", regex=True)

        df.insert(0, "Ma_NH", ma_nh)
        with pd.ExcelWriter("clean/crl_selenium.xlsx", engine='openpyxl', mode='a', if_sheet_exists='replace') as writer:
            df.to_excel(writer, encoding="utf-8"+u"",
                        sheet_name=f'{bank_name}')
        driver.close()
        countt = countt-1

    except AttributeError as python_error:
        print("Bug python", python_error)
        Err_sys = str(python_error)
        Err_sys = Err_sys.replace("'", "...")
        check_compare = 'error'
        error = (
            f'Bank Error: {bank_name}, type crawl: {type_bank}, action: {action_bank}')
        print(error)
        Log_dbOracle.write_log(
            check_point, check_compare, result, Err_sys, error)
        connection.rollback()

    except NoSuchElementException as Xpath_error:
        print("Bug Xpath", Xpath_error)
        Err_sys = str(Xpath_error)
        check_compare = 'error'
        error = (
            f'Bank Error: {bank_name}, type crawl: {type_bank}, action: {action_bank}')
        print(error)
        Log_dbOracle.write_log(
            check_point, check_compare, result, Err_sys, error)
        connection.rollback()
