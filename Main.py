from datetime import datetime
import multiprocessing
import Info_preparation
from joblib import Parallel, delayed
import Parser_pdf_html
import Log_dbOracle
from clean.clean import *

inputs = range(10)


def processInput(i):
    print(i * i)


num_cores = multiprocessing.cpu_count()
print(num_cores)


if __name__ == "__main__":
    # Begin
    now = datetime.now()
    print("now =", now)
    dt_string = now.strftime("%d/%m/%Y %H:%M:%S")

    # Start Code Preparation
    Info_preparation.Pdf_info()
    print("\033[92m{}\033[00m" .format('Step: Prepare_PDF ++FINISH++'))

    Info_preparation.Web_info()
    print("\033[92m{}\033[00m" .format('Step: Prepare_Web ++FINISH++'))

    Info_preparation.download_file_pdf()
    print("\033[92m{}\033[00m" .format('Step: Prepare_dowload ++FINISH++'))

    # Info_preparation
    Info_preparation.lists_pdf()
    print("\033[92m{}\033[00m" .format('Step: List ++FINISH++'))

    # print(Info_preparation.web_ATTRIBUTE)
    bank_pdf = Info_preparation.Bank_pdf
    bank_web = Info_preparation.Bank_web

    leng_pdf = range(len(bank_pdf))
    leng_web = range(len(bank_web))

    #Parallel Download pdf
    # Parallel_Pdf = Parallel(n_jobs=num_cores, backend="threading")(
    #      delayed(Info_preparation.download_file_pdf)(i) for i in leng_pdf)

    #Paser file pdf with parallel
    Parallel_Parser_pdf = Parallel(n_jobs=num_cores)(delayed(Parser_pdf_html.paser_pdf)(
        Info_preparation.pdf_ma_nh[i], Info_preparation.listt[i], Info_preparation.pdf_PAGE_NUM[i],
        Info_preparation.pdf_TABLE_NUM[i], Info_preparation.pdf_BANK_NAME[i]) for i in leng_pdf)
    print("\033[92m{}\033[00m" .format('Step: Parser PDF ++FINISH++'))

    #Paser html with parallel
    Parallel_Parser_html = Parallel(n_jobs=num_cores, backend="threading")(
        delayed(Parser_pdf_html.paser_html)(i) for i in leng_web)
    print("\033[92m{}\033[00m" .format('Step: Parser WEB ++FINISH++'))

    # Check write log
    if(Parser_pdf_html.countt == 0):
        Log_dbOracle.write_log(4, '', '', '', '')
    print("\033[92m{}\033[00m" .format('Step: Write Log ++FINISH++'))

    # Clean data
    clean_all()
    print("\033[92m{}\033[00m" .format('Step: Clean ++FINISH++'))
    
    # End
    now1 = datetime.now()
    dt_string1 = now1.strftime("%d/%m/%Y %H:%M:%S")
    print(f'start time: {dt_string}')
    print(f'end time: {dt_string1}')
