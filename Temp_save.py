### PDF
def list_path_pdf(self):
    pdf_acb = "D:/HuyDq/crawl/1_acb.pdf"
    pdf_sacombank = "D:/HuyDq/crawl/2_sacombank.pdf"
    pdf_techcombank = "D:/HuyDq/crawl/3_techcombank.pdf"
    pdf_vpbank = "D:/HuyDq/crawl/4_vpbank.pdf"
    pdf_hdbank = "D:/HuyDq/crawl/5_hdbank.pdf"
    pdf_vietbank = "D:/HuyDq/crawl/6_vietbank.pdf"
    pdf_shb = "D:/HuyDq/crawl/7_shb.pdf"
    pdf_hsbc = "D:/HuyDq/crawl/8_hsbc.pdf"

#### IMAGE
def list_url_anh(self):
    url_dongabank = "https://www.dongabank.com.vn/upload/lib/tttt-vnd.pdf?=1636360213"
    url_vietabank = "https://www.vietabank.com.vn/uploads/files/truy-cap-nhanh/lai-suat-tien-gui/ls1-12.08.2021.jpg"
    url_shb = "https://www.shb.com.vn/wp-content/uploads/2020/08/01.Bieu-LS-H%C4%90-KHCN-VND-11.10.2021-final.pdf"
    url_sc = "https://av.sc.com/vn/content/docs/vn-interest-rate-personal-banking-vn.pdf"

### big list pdf bank bao gồm bank và nhũng thuộc tính đi kèm: mô tả bên dưới
bank_pdf_temp = [
    ['79307001'                             ## mã ngân hàng
    ,'https://acb.com.vn/wps/wcm/connect/6b0ba300-084f-4133-8773-58040a2863a9/Bang%2Btong%2Bhop%2Blai%2Bsuat_161021+%281%29.pdf?MOD=AJPERES'             ## đường dẫn đến pdf
    ,'PDF'                                  ## kiểu là pdf
    ,'1'                                    ## số thự tự trang pdf cần tách
    ,'0'                                    ## số thự tự bảng cần lấy data
    ,'Acb'],                                ## tên ngân hàng để lưu tên sheet 

    ['79303001'
    ,'https://www.sacombank.com.vn/canhan/Documents/LaiSuat/LAI_SUAT_NIEM_YET_TAI_QUAY_19.10.2021.pdf'
    ,'PDF'
    ,'1'
    ,'0'
    ,'Sacomank'],

    ['79310001'
    ,'https://www.techcombank.com.vn/file/get/24755'
    ,'PDF'
    ,'1'
    ,'0'
    ,'Techcombank'],

    ['79309001'
    ,'https://www.vpbank.com.vn/-/media/vpbank-latest/tai-lieu-bieu-mau/lai-suat-huy-dong/biu-li-sut--ti-quy-2021-07-03.pdf'
    ,'PDF'
    ,'1'
    ,'0'
    ,'Vpbank'],

    ['79321001'
    ,'https://hdbank.com.vn/api/download?fileName=document//20210824BIEULAISUATTIENGUIKHACHHANGCANHANfinal__1629964177938.pdf'
    ,'PDF'
    ,'1'
    ,'0'
    ,'Hdbank'],

    # ['79356001'
    # ,'D:/HuyDq/CRL_BANK_RATE/pdf/6_vietbank.pdf'
    # ,'PDF'
    # ,'1'
    # ,'0'
    # ,'Vietbank'],

    ['79348001'
    ,'https://www.shb.com.vn/wp-content/uploads/2020/08/01.Bieu-LS-H%C4%90-KHCN-VND-17.11.2021-final-1.pdf'
    ,'PDF'
    ,'1'
    ,'1'
    ,'Shb'],

    ['79617001'
    ,'https://www.hsbc.com.vn/content/dam/hsbc/hbvn/documents/vi/rates/HSBC_INTEREST_RATES_VN.pdf'
    ,'PDF'
    ,'1'
    ,'0'
    ,'HSBC']
    ]

bank_web_temp =  [
    ['79319001'                                                             ## mã ngân hàng
    ,'http://oceanbank.vn/lai-suat/19/tiet-kiem-thuong.html'                ## đường dẫn đến trang web
    ,'Web'                                                                  ## kiểu là web
    ,'//*[@id="p-rate"]/div/div/div[2]/div/div[2]/div/table'                ## xpath đến table
    ,'0'                                                                    ## có thao tác trên trang web 1 là có, 0 là không
    ,'Oceanbank'],                                                          ## tên ngân hàng

    ['01204001'
    ,'https://www.agribank.com.vn/vn/lai-suat'
    ,'Web'
    ,'//*[@id="laiSuatCn"]/table'
    ,'0'
    ,'Agribank'],

    ['79302001'
    ,'https://www.msb.com.vn/Bieu-mau-Bieu-phi-va-Lai-suat/1177/3/Lai-suat'
    ,'Web'
    ,'//*[@id="table3"]'
    ,'0'
    ,'MSB'],

    ['79357001'
    ,'https://lienvietpostbank.com.vn/lai-suat'
    ,'Web'
    ,'//*[@id="Wrapper"]/div[2]/div[3]/div[1]/div[1]/table[2]'
    ,'0'
    ,'Lienvietpostbank'],

    ['79333001'
    ,'https://www.ocb.com.vn/vi/cong-cu/lai-suat'
    ,'Web'
    ,'/html/body/app-root/app-master/main/app-lai-suat/div/div/div/div[2]/div[3]/div[2]/table'
    ,'0'
    ,'Ocb'],

    ['79320001'
    ,'https://www.gpbank.com.vn/InterestDetail'
    ,'Web'
    ,'//*[@id="divInterestDetail1"]/div[2]/table'  
    ,'0'
    ,'Gpbank'],

    ['970444'
    ,'https://www.cbbank.vn/Pages/InterestRate.aspx'
    ,'Web'
    ,'//*[@id="ContentPlaceHolder1_Panel1"]/div[1]/div/div[2]/div[1]/table'
    ,'0'
    ,'Cbbank'],

    ['79353001'
    ,'https://laisuat.kienlongbank.com/lai-suat-ca-nhan'
    ,'Web'
    ,'//*[@id="ctl00_mainContent_ctl02_up"]/div/div/section/article/div/table[2]'
    ,'0'
    ,'Kienlongbank'],

    ['79341001'
    ,'https://www.pgbank.com.vn/lai-suat-tiet-kiem/ca-nhan-vnd'
    ,'Web'
    ,'//*[@id="__layout"]/div/div[2]/div[3]/div[2]/div/table'
    ,'0'
    ,'Pgbank'],

    ['970415'
    ,'https://www.vietinbank.vn/web/home/vn/lai-suat'
    ,'Web'
    ,'//*[@id="hor-ex-b"]'
    ,'0'
    ,'Vietinbank'],

    ['79327001'
    ,'https://www.vietcapitalbank.com.vn/personal'
    ,'Web'
    ,'//*[@id="tool-table2"]/div[3]/div/div[1]/div/table'
    ,'0'
    ,'Vietcapitalbank'],

    ['01202001'
    ,'https://www.bidv.com.vn/vn/tra-cuu-lai-suat'
    ,'Web'
    ,'//*[@id="rates"]/div/table'
    ,'0'
    ,'Bidv'],

    ['79352001'
    ,'https://www.ncb-bank.vn/vi/lai-suat-tien-gui'
    ,'Web'
    ,'//*[@id="tab-laisuat"]/div/div[2]/div[1]/table[1]'
    ,'0'
    ,'Ncb'],

    ['79203001'
    ,'https://portal.vietcombank.com.vn/LS/Pages/lai-suat.aspx?devicechannel=default'
    ,'Web'
    ,'//*[@id="danhsachlaisuat"]'
    ,'0'
    ,'Vietcombank'],

    ['79306001'
    ,'https://www.namabank.com.vn/lai-suat'
    ,'Web'
    ,'//*[@id="ctl00_divCenter"]/div[2]/div/section/div/div[3]/div/table'
    ,'0'
    ,'NamA'],

    ['79359001'
    ,'https://www.baovietbank.vn/vn/lai-suat'
    ,'Web'
    ,'//*[@id="colap0"]/div/div[1]/table'
    ,'0'
    ,'Baovietbank'],

    ['01313007'
    ,'https://www.baca-bank.vn/SitePages/website/lai-xuat.aspx?ac=L%u00e3i+su%u1ea5t&s=LX'
    ,'Web'
    ,'/html/body'
    ,'1'
    ,'BacA'
    ,'//*[@id="ctl00_PlaceHolderMain_g_3d987530_2758_4587_b052_bda8fbe88390_ctl00_DDL_LS"]'
    ,'//*[@id="ctl00_PlaceHolderMain_g_3d987530_2758_4587_b052_bda8fbe88390_ctl00_DDL_LS"]/option[2]'],

    ['79360001'
    ,'https://www.pvcombank.com.vn/bieu-lai-suat'
    ,'Web'
    ,'/html/body'
    ,'1'
    ,'Pvcombank'
    ,'//*[@id="slSavingProduct"]'
    ,'//*[@id="slSavingProduct"]/option[5]'
    ,'//*[@id="savingProductName"]'],

    ['79308001'
    ,'https://www.saigonbank.com.vn/vi/truy-cap-nhanh/lai-suat/Lai-suat-tien-gui-tiet-kiem'
    ,'Web'
    ,'//*[@id="main-container"]/div/div[3]/div[1]/div/div[2]/div[4]/div[4]/div[3]/table'
    ,'0'
    ,'Saigonbank'],

    ['79358001'
    ,'https://tpb.vn/cong-cu-tinh-toan/lai-suat'
    ,'Web'
    ,'//*[@id="tab-1"]/table[1]'
    ,'0'
    ,'Tpbank']
    ]