from joblib import Parallel, delayed
import multiprocessing
import threading
from selenium import webdriver
import urllib3
import re
import time
import pandas as pd

import bs4 as bs
from collections import defaultdict
from IPython.display import display
from datetime import datetime
from itertools import product
import camelot
from camelot.io import read_pdf
import numpy as np
import ssl
from IPython.display import display
import cx_Oracle
from logging import exception
from sys import excepthook
from selenium import webdriver
import urllib3
import re
import time
import pandas as pd
import bs4 as bs
from collections import defaultdict
from selenium.webdriver.chrome.options import Options
from itertools import product
from selenium.common.exceptions import NoSuchElementException
from os import name
from sys import path
import urllib.request
from datetime import date
from http.client import IncompleteRead
import os
from datetime import datetime
# what are your inputs, and what operation do you want to
# perform on each input. For example...

now = datetime.now()
print("now =", now)

# dd/mm/YY H:M:S
dt_string = now.strftime("%d/%m/%Y %H:%M:%S")

# Create driver object. Opens browser window.
path = "chromedriver.exe"
option = webdriver.ChromeOptions()

option.add_argument("--headless")
prefs = {"profile.managed_default_content_settings.images": 2}
option.add_experimental_option("prefs", prefs)


bank_web_temp =  [
    ['79319001'                                                             ## mã ngân hàng
    ,'http://oceanbank.vn/lai-suat/19/tiet-kiem-thuong.html'                ## đường dẫn đến trang web
    ,'Web'                                                                  ## kiểu là web
    ,'//*[@id="p-rate"]/div/div/div[2]/div/div[2]/div/table'                ## xpath đến table
    ,'0'                                                                    ## có thao tác trên trang web 1 là có, 0 là không
    ,'Oceanbank'],                                                          ## tên ngân hàng

    ['01204001'
    ,'https://www.agribank.com.vn/vn/lai-suat'
    ,'Web'
    ,'//*[@id="laiSuatCn"]/table'
    ,'0'
    ,'Agribank'],

    ['79302001'
    ,'https://www.msb.com.vn/Bieu-mau-Bieu-phi-va-Lai-suat/1177/3/Lai-suat'
    ,'Web'
    ,'//*[@id="table3"]'
    ,'0'
    ,'MSB'],

    ['79357001'
    ,'https://lienvietpostbank.com.vn/lai-suat'
    ,'Web'
    ,'//*[@id="Wrapper"]/div[2]/div[3]/div[1]/div[1]/table[2]'
    ,'0'
    ,'Lienvietpostbank'],

    ['79333001'
    ,'https://www.ocb.com.vn/vi/cong-cu/lai-suat'
    ,'Web'
    ,'/html/body/app-root/app-master/main/app-lai-suat/div/div/div/div[2]/div[3]/div[2]/table'
    ,'0'
    ,'Ocb'
    ],

    ['79320001'
    ,'https://www.gpbank.com.vn/InterestDetail'
    ,'Web'
    ,'//*[@id="divInterestDetail1"]/div[2]/table'  
    ,'0'
    ,'Gpbank'],

    ['970444'
    ,'https://www.cbbank.vn/Pages/InterestRate.aspx'
    ,'Web'
    ,'//*[@id="ContentPlaceHolder1_Panel1"]/div[1]/div/div[2]/div[1]/table'
    ,'0'
    ,'Cbbank'],

    ['79353001'
    ,'https://laisuat.kienlongbank.com/lai-suat-ca-nhan'
    ,'Web'
    ,'//*[@id="ctl00_mainContent_ctl02_up"]/div/div/section/article/div/table[2]'
    ,'0'
    ,'Kienlongbank'],

    ['79341001'
    ,'https://www.pgbank.com.vn/lai-suat-tiet-kiem/ca-nhan-vnd'
    ,'Web'
    ,'//*[@id="__layout"]/div/div[2]/div[3]/div[2]/div/table'
    ,'0'
    ,'Pgbank'],

    ['970415'
    ,'https://www.vietinbank.vn/web/home/vn/lai-suat'
    ,'Web'
    ,'//*[@id="hor-ex-b"]'
    ,'0'
    ,'Vietinbank'],

    ['79327001'
    ,'https://www.vietcapitalbank.com.vn/personal'
    ,'Web'
    ,'//*[@id="tool-table2"]/div[3]/div/div[1]/div/table'
    ,'0'
    ,'Vietcapitalbank'],

    # ['01202001'
    # ,'https://www.bidv.com.vn/vn/tra-cuu-lai-suat'
    # ,'Web'
    # ,'//*[@id="rates"]/div/table'
    # ,'0'
    # ,'Bidv'],

    ['79352001'
    ,'https://www.ncb-bank.vn/vi/lai-suat-tien-gui'
    ,'Web'
    ,'//*[@id="tab-laisuat"]/div/div[2]/div[1]/table[1]'
    ,'0'
    ,'Ncb'],

    ['79203001'
    ,'https://portal.vietcombank.com.vn/LS/Pages/lai-suat.aspx?devicechannel=default'
    ,'Web'
    ,'//*[@id="danhsachlaisuat"]'
    ,'0'
    ,'Vietcombank'],

    ['79306001'
    ,'https://www.namabank.com.vn/lai-suat'
    ,'Web'
    ,'//*[@id="ctl00_divCenter"]/div[2]/div/section/div/div[3]/div/table'
    ,'0'
    ,'NamA'],

    ['79359001'
    ,'https://www.baovietbank.vn/vn/lai-suat'
    ,'Web'
    ,'//*[@id="colap0"]/div/div[1]/table'
    ,'0'
    ,'Baovietbank'],

    ['79308001'
    ,'https://www.saigonbank.com.vn/vi/truy-cap-nhanh/lai-suat/Lai-suat-tien-gui-tiet-kiem'
    ,'Web'
    ,'//*[@id="main-container"]/div/div[3]/div[1]/div/div[2]/div[4]/div[4]/div[3]/table'
    ,'0'
    ,'Saigonbank'],

    ['79358001'
    ,'https://tpb.vn/cong-cu-tinh-toan/lai-suat'
    ,'Web'
    ,'//*[@id="tab-1"]/table[1]'
    ,'0'
    ,'Tpbank']
    ]

def read_web_info_txt():
    global bank_web
    bank_web=[]
    my_file = open("web_info.txt", "r",encoding="utf8")
    content = my_file.read()
    content_list = content.split("\n")
    my_file.close()
    list_index=[]
    temp=[]
    leng_content=len(content_list)
    for i in range(leng_content):
        if content_list[i]=='':
            list_index.append(i)
    leng_index=len(list_index)
    n=0
    for i in range(leng_index):
        for j in range (n,list_index[i]):
            a=content_list[j].replace(" ","")
            temp.append(a)
        bank_web.append(temp)
        temp=[]
        n=list_index[i]+1
### big list web bank bao gồm bank và nhũng thuộc tính đi kèm: mô tả bên dưới
def read_pdf_info_txt():
    global bank_pdf
    bank_pdf=[]
    my_file = open("pdf_info.txt", "r",encoding="utf8")
    content = my_file.read()
    content_list = content.split("\n")
    my_file.close()

    list_index=[]
    temp=[]
    leng_content=len(content_list)
    for i in range(leng_content):
        if content_list[i]=='':
            list_index.append(i)

    leng_index=len(list_index)
    n=0
    for i in range(leng_index):
        for j in range (n,list_index[i]):
            a=content_list[j].replace(" ","")
            temp.append(a)
        bank_pdf.append(temp)
        temp=[]
        n=list_index[i]+1

### hàm parser theo đúng hiển thị table ở trên web chia cột rõ ràng  
def table_to_2d(table_tag):
    rowspans = []  # track pending rowspans
    rows = table_tag.find_all('tr')

    # first scan, see how many columns we need
    colcount = 0
    for r, row in enumerate(rows):
        cells = row.find_all(['td', 'th'], recursive=False)
        # count columns (including spanned).
        # add active rowspans from preceding rows
        # we *ignore* the colspan value on the last cell, to prevent
        # creating 'phantom' columns with no actual cells, only extended
        # colspans. This is achieved by hardcoding the last cell width as 1. 
        # a colspan of 0 means “fill until the end” but can really only apply
        # to the last cell; ignore it elsewhere. 
        colcount = max(
            colcount,
            sum(int(c.get('colspan', 1)) or 1 for c in cells[:-1]) + len(cells[-1:]) + len(rowspans))
        # update rowspan bookkeeping; 0 is a span to the bottom. 
        rowspans += [int(c.get('rowspan', 1)) or len(rows) - r for c in cells]
        rowspans = [s - 1 for s in rowspans if s > 1]

    # it doesn't matter if there are still rowspan numbers 'active'; no extra
    # rows to show in the table means the larger than 1 rowspan numbers in the
    # last table row are ignored.

    # build an empty matrix for all possible cells
    table = [[None] * colcount for row in rows]

    # fill matrix from row data
    rowspans = {}  # track pending rowspans, column number mapping to count
    for row, row_elem in enumerate(rows):
        span_offset = 0  # how many columns are skipped due to row and colspans 
        for col, cell in enumerate(row_elem.find_all(['td', 'th'], recursive=False)):
            # adjust for preceding row and colspans
            col += span_offset
            while rowspans.get(col, 0):
                span_offset += 1
                col += 1

            # fill table data
            rowspan = rowspans[col] = int(cell.get('rowspan', 1)) or len(rows) - row
            colspan = int(cell.get('colspan', 1)) or colcount - col
            # next column is offset by the colspan
            span_offset += colspan - 1
            value = cell.get_text()
            for drow, dcol in product(range(rowspan), range(colspan)):
                try:
                    table[row + drow][col + dcol] = value
                    rowspans[col + dcol] = rowspan
                except IndexError:
                    # rowspan or colspan outside the confines of the table
                    pass

        # update rowspan bookkeeping
        rowspans = {c: s - 1 for c, s in rowspans.items() if s > 1}

    return table


def main_web(ma_nh,url,xpath,name):

    driver = webdriver.Chrome(executable_path=path, options=option)
    
    driver.get(f"{url}")
    time.sleep(10)
    elem = driver.find_element_by_xpath(f'{xpath}')


    
    # if(leng>6):
    #     # cho vòng for để thực hiện chuỗi thao tác   
             
    #     for i in range(6,leng):
    #         options = each_bank[i]
    #         loadMore =driver.find_element_by_xpath(xpath=f'{options}')  
    #         loadMore.click()
    #         time.sleep(1)

    #     # for xong thì lấy lại xpath lần nữa
    #     elem = driver.find_element_by_xpath(f'{xpath_bank}')




    source_code = elem.get_attribute("outerHTML")
    soup = bs.BeautifulSoup(source_code,'lxml')

    table_parser=table_to_2d(soup.table)

    df=pd.DataFrame(table_parser)
    
    len_num=len(df.columns)
    for i in range (len_num):
        
        df[i] = df[i].str.replace("\n","")
        df[i] = df[i].str.replace("\t","")
        df[i] = df[i].str.replace("(","")
        df[i] = df[i].str.replace(")","")
        df[i] = df[i].str.replace("%","")
        df[i] = df[i].str.replace("*","")
        df[i] = df[i].str.replace(",",".")
        df[i] = df[i].str.replace("  ","")

    df.insert(0,"Ma_NH",ma_nh)
    print(name)
    display(df)
    # with pd.ExcelWriter("clen/crl_selenium.xlsx", engine='openpyxl',mode='a') as writer: 
    #     df.to_excel(writer,encoding="utf-8"+u"",sheet_name=f'{name}')
    driver.close()


def download_file_pdf(path,name):
    opener = urllib.request.URLopener()
    opener.addheader('User-Agent', 'whatever')
    
    today = date.today()
    pdf_date= today.strftime("%d/%m/%Y")
    pdf_date=str(pdf_date).replace("/","_")
    global listt
    listt=[]

    try:
        PATH = path                  
        NAME = name
        PATH_DESC = F'pdf/{NAME}_{pdf_date}.pdf'
        listt.append(PATH_DESC)
        print(PATH_DESC)
        filename, headers = opener.retrieve(f'{PATH}',f'{PATH_DESC}')

            #urllib.request.urlretrieve(f'{URL}',f'{PATH_DESC}')
    except IncompleteRead:
        if len(listt)>0:
            for i in range(len(listt)):
                os.remove(f'{listt[i]}')
            listt=[]
            PATH = path                  
            NAME = name
            PATH_DESC = F'pdf/{NAME}_{pdf_date}.pdf'
            listt.append(PATH_DESC)
            print(PATH_DESC)
            filename, headers = opener.retrieve(f'{PATH}',f'{PATH_DESC}')

        else:
 
            PATH = path                  
            NAME = name
            PATH_DESC = F'pdf/{NAME}_{pdf_date}.pdf'
            listt.append(PATH_DESC)
            print(PATH_DESC)
            filename, headers = opener.retrieve(f'{PATH}',f'{PATH_DESC}')
    except urllib.error.URLError as a:
        print(a)
pdf_ma_nh=[]
pdf_PATH=[]
pdf_PAGE_NUM=[]
pdf_TABLE_NUM=[]
pdf_BANK_NAME=[]
pdf_TYPE=[]

list_ma_nh=[]
list_URL=[]
list_ATTRIBUTE=[]
list_ACTION=[]
list_BANK_NAME=[]

def lists_pdf():
    leng_pdf=range(len(bank_pdf))
    for i in leng_pdf:
            
        MASTER=bank_pdf[i]
        MA_NH=MASTER[0]
        PATH=MASTER[1]
        TYPE=MASTER[2]
        PAGE_NUM=MASTER[3]
        TABLE_NUM=MASTER[4]
        BANK_NAME=MASTER[5]
        VALUE_MASTER=[MA_NH,PATH,TYPE,PAGE_NUM,TABLE_NUM]
        #print(VALUE_MASTER)
        pdf_ma_nh=[]

        pdf_ma_nh.append(MA_NH)
        pdf_PATH.append(PATH)
        pdf_TYPE.append(TYPE)
        pdf_PAGE_NUM.append(PAGE_NUM)
        pdf_TABLE_NUM.append(TABLE_NUM)
        pdf_BANK_NAME.append(BANK_NAME)

if __name__ == "__main__":
        
    now = datetime.now()
    print("now =", now)

    read_web_info_txt()
    read_pdf_info_txt()
    # leng=range(len(bank_web))
    # for i in leng:
            
    #     MASTER=bank_web[i]
    #     MA_NH=MASTER[0]
    #     URL=MASTER[1]
    #     TYPE=MASTER[2]
    #     ATTRIBUTE=MASTER[3]
    #     ACTION=MASTER[4]
    #     BANK_NAME=MASTER[5]
    #     VALUE_MASTER=[MA_NH,URL,TYPE,ATTRIBUTE,ACTION]
    #     #print(VALUE_MASTER)
    #     list_ma_nh.append(MA_NH)
    #     list_URL.append(URL)
    #     list_ATTRIBUTE.append(ATTRIBUTE)
    #     list_ACTION.append(ACTION)
    #     list_BANK_NAME.append(BANK_NAME)
    leng_pdf=range(len(bank_pdf))
    lists_pdf()
    results = Parallel(n_jobs=num_cores)(delayed(download_file_pdf)(pdf_PATH[i],pdf_BANK_NAME[i]) for i in leng_pdf)
    #results = Parallel(n_jobs=num_cores)(delayed(main_web)(list_ma_nh[i],list_URL[i],list_ATTRIBUTE[i],list_BANK_NAME[i]) for i in leng)


    now1 = datetime.now()
    print("before =",now)
    print("now =", now1)

    # dd/mm/YY H:M:S
    dt_string1 = now1.strftime("%d/%m/%Y %H:%M:%S")