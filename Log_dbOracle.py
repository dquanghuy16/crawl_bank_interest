import cx_Oracle

def write_log(check_point, check_compare, result, Err_sys, error):

    connection = cx_Oracle.connect(
        'dwprod/oracle123@192.168.49.32:1521/uatdwh')
    print("Database version:", connection.version)
    cur = connection.cursor()

    sql = f"""declare
    l_start_time varchar(20); --luu start time de update dung dong log ODI
    l_error varchar2(5000); --chi tiet loi
    l_num_record int;
    V_PREVIOUS_DATE date;
    p_holi_day date;
    l_today date;
    l_check_point number;
    l_check_temp number;
    l_check_compare nvarchar2(50);

    BEGIN
    
    l_check_point :=1;
    l_today := trunc(sysdate);
    l_start_time := OBI.PKG_ODI_LOG.FN_SET_ODI_START('82_1' --P_ORD
                                                    ,'GET_BANK_RAATE' --P_ODI_NAME
                                                    ,'GET_BANK_RAATE' --P_PROC_NAME
                                                    ,l_today --P_ODI_DATE
    /*BEGIN PYTHON */                                               );
    l_check_point :={check_point};
    l_check_compare:='{check_compare}';
    l_check_temp:=l_check_compare;                                            
    DBMS_OUTPUT.put_line('{result}'); 
    

    --GHI LOG LOI HE THONG
       
    /*END PYTHON*/
        OBI.PKG_ODI_LOG.SP_SET_ODI_END('82_1' --P_ORD
                                ,'GET_BANK_RAATE' --P_ODI_NAME
                                ,'GET_BANK_RAATE' --P_PROC_NAME
                                ,l_today --P_ODI_DATE
                                ,l_start_time --P_START_TIME
                                );
    EXCEPTION
    when others then
        l_error := '{Err_sys}'||' At point :'||l_check_point || ' and {error}' ; --Lay loi he thong
        DBMS_OUTPUT.put_line(l_error);
            OBI.PKG_ODI_LOG.SP_SET_ODI_ERROR('82_1' --P_ORD
                                ,'GET_BANK_RAATE' --P_ODI_NAME
                                ,'GET_BANK_RAATE' --P_PROC_NAME
                                ,l_today --P_ODI_DATE
                                ,l_start_time --P_START_TIME
                                ,l_error --P_DESC
                                );                          
    end;  """
    cur.execute(sql)
