from types import coroutine
import pandas as pd
from datetime import date
import numpy as np
import re


def clean_ocean(data):
    day = date.today().strftime('%d/%m/%Y')
    data.columns = ['x', 'MA_NH', 'TERM', 'QUAY', 'ONLINE', 'USD']
    data = data[['MA_NH', 'TERM', 'QUAY', 'ONLINE', 'USD']]
    data = data.loc[3:, :]
    temp = data.drop(columns='USD').melt(
        id_vars=['MA_NH', 'TERM'], var_name='TYPE', value_name='INTEREST')
    temp.columns = ['MA_NH', 'TERM', 'TYPE', 'VND']
    temp['USD'] = 0
    result = temp.melt(id_vars=['MA_NH', 'TERM', 'TYPE'],
                       var_name='CURRENCY', value_name='INTEREST')
    result.columns = ['MA_NH', 'TERM', 'TYPE', 'CURRENCY', 'INTEREST']
    result['TYPE_OF_DEPOSIT'] = result['TERM'].apply(
        lambda x: 'CKH' if x != 'Không kỳ hạn' else 'KKH')
    result['DATE'] = day
    result['TYPE_OF_INTEREST'] = 'CK'
    result['SEGMENT'] = 'KPB'
    result['PRODUCT'] = 'ONE PRODUCT'
    
    result['TERM'] = result['TERM'].apply(lambda x: x.lstrip('0').replace(
        " tháng", 'M') if 'tháng' in x else x.lstrip('0').replace(' tuần', 'W'))
    result['TERM'] = result['TERM'].apply(
        lambda x: 'KKH' if x == 'Không kỳ hạn' else x)
    result['AMOUNT']  = 'KPB'
    result = result[['MA_NH', 'PRODUCT', 'TERM', 'CURRENCY', 'TYPE_OF_INTEREST',
                     'TYPE', 'TYPE_OF_DEPOSIT', 'SEGMENT','AMOUNT', 'INTEREST', 'DATE']]
    result['INTEREST'] = result['INTEREST'].astype(float)
    result.reset_index(inplace=True, drop=True)
    return result


def clean_ocb(data):
    day = date.today().strftime('%d/%m/%Y')
    data.columns = ['x', 'MA_NH', 'TERM', 'TIETKIEM-CKH',
                    'TIETKIEM-THONGTHUONG', 'TIETKIEM-ONLINE']
    temp = data[['MA_NH', 'TERM', 'TIETKIEM-CKH',
                 'TIETKIEM-THONGTHUONG', 'TIETKIEM-ONLINE']]
    temp = temp.loc[1:, :]
    temp['TERM'] = temp['TERM'].apply(lambda x: x.replace(
        ' Tháng', 'M') if 'Tháng' in x else 'KKH')
    result = temp.melt(id_vars=['MA_NH', 'TERM'],
                       var_name='PRODUCT', value_name='INTEREST')
    result['SEGMENT'] = 'KHCN'
    result['TYPE_OF_INTEREST'] = 'CK'
    result['TYPE_OF_DEPOSIT'] = result['PRODUCT'].apply(
        lambda x: 'CKH' if 'CKH' in x else 'TK')
    result['CURRENCY'] = 'VND'
    result['DATE'] = day
    result['TYPE'] = result['PRODUCT'].apply(
        lambda x: 'ONLINE' if 'ONLINE' in x else 'QUAY')
    result['AMOUNT']  = 'KPB'
    result = result[['MA_NH', 'PRODUCT', 'TERM', 'CURRENCY', 'TYPE_OF_INTEREST',
                     'TYPE', 'TYPE_OF_DEPOSIT', 'SEGMENT','AMOUNT', 'INTEREST', 'DATE']]
    result['INTEREST'] = result['INTEREST'].astype(float)
    result.reset_index(inplace=True, drop=True)
    return result


def clean_gpbank(data):
    def map(x):
        if 'tháng' in x:
            return x.replace(' tháng', 'M')
        elif 'Dưới' in x:
            return x.replace('Dưới ', '<').replace(' tuần', 'W')
        elif 'tuần' in x:
            return x.replace(' tuần', 'W')
    day = date.today().strftime('%d/%m/%Y')
    data.columns = ['x', 'MA_NH', 'TERM', 'DK', '1M', '3M', '6M', '12M', 'CK']
    temp = data[['MA_NH', 'TERM', 'DK', '1M', '3M', '6M', '12M', 'CK']]
    temp = temp.iloc[2:, :]
    result = temp.melt(id_vars=['MA_NH', 'TERM'],
                       var_name='TYPE_OF_INTEREST', value_name='INTEREST')
    result = result[~result['INTEREST'].isin(['-'])]
    result['TERM'] = result['TERM'].apply(lambda x: map(x))
    result['CURRENCY'] = 'VND'
    result['TYPE'] = 'QUAY'
    result['TYPE_OF_DEPOSIT'] = result['TERM'].apply(
        lambda x: 'KKH' if x == 'KKH' else x)
    result['SEGMENT'] = 'KPB'
    result['DATE'] = day
    result['PRODUCT'] = 'ONE PRODUCT'
    result['AMOUNT']  = 'KPB'
    result = result[['MA_NH', 'PRODUCT', 'TERM', 'CURRENCY', 'TYPE_OF_INTEREST',
                     'TYPE', 'TYPE_OF_DEPOSIT', 'SEGMENT','AMOUNT', 'INTEREST', 'DATE']]
    result['INTEREST'] = result['INTEREST'].astype(float)
    result.reset_index(inplace=True, drop=True)
    return result


def clean_pgbank(data):
    day = date.today().strftime('%d/%m/%Y')
    data.columns = ['x', 'MA_NH', 'TERM', 'CK', '1M']
    temp = data[['MA_NH', 'TERM', 'CK', '1M']]
    temp = temp.loc[1:, :]
    result = temp.melt(id_vars=['MA_NH', 'TERM'],
                       var_name='TYPE_OF_INTEREST', value_name='INTEREST')
    result.dropna(inplace=True)
    result['TERM'] = result['TERM'].apply(lambda x: x.replace(
        ' Ngày', 'D') if 'Ngày' in x else x.replace(' Tháng', 'M'))
    result['PRODUCT'] = 'ONE PRODUCT'
    result['CURRENCY'] = 'VND'
    result['TYPE'] = 'QUAY'
    result['TYPE_OF_DEPOSIT'] = 'TK'
    result['SEGMENT'] = 'KPB'
    result['DATE'] = day
    result['AMOUNT']  = 'KPB'
    result = result[['MA_NH', 'PRODUCT', 'TERM', 'CURRENCY', 'TYPE_OF_INTEREST',
                     'TYPE', 'TYPE_OF_DEPOSIT', 'SEGMENT','AMOUNT', 'INTEREST', 'DATE']]
    result['INTEREST'] = result['INTEREST'].astype(float)
    result.reset_index(inplace=True, drop=True)
    return result


def clean_cbbank(data):
    interest_kkh = float(re.findall(r'\d+\.\d+',data.iloc[0,2])[0])
    day = date.today().strftime('%d/%m/%Y')    
    data = data.loc[4:,:].copy()
    data.loc[:,'TERM'] = data.loc[:,0] + data.loc[:,1]
    data.columns = ['x', 'MA_NH', 0, 1, 'CK', '1M', '3M', 'DK', 6, 'TERM']
    temp       = data[['MA_NH','CK','1M','3M','DK','TERM']].copy()
    temp['TERM'] = temp['TERM'].apply(lambda x: x.replace('tuần','W') if 'tuần' in x else x.replace('tháng','M'))
    result       = temp.melt(id_vars = ['MA_NH','TERM'], var_name = 'TYPE_OF_INTEREST', value_name = 'INTEREST')
    result['INTEREST'] = result['INTEREST'].str.strip()
    result['INTEREST'] = result['INTEREST'].apply(lambda x:  np.nan if x == '' else x)
    result.dropna(inplace = True)
    result['INTEREST'] = result['INTEREST'].astype('float')
    result.reset_index(drop = True, inplace = True)
    result.loc[len(result.index)] = [970444,'KKH','KKH',interest_kkh]
    result['PRODUCT']  = 'ONE PRODUCT'
    result['CURRENCY'] = 'VND'
    result['TYPE']     = 'QUAY'
    result['TYPE_OF_DEPOSIT'] = result['TYPE_OF_INTEREST'].apply(lambda x: 'KKH' if x == 'KKH' else 'CKH')
    result['SEGMENT']  = 'KPB'
    result['DATE']     = day
    result['AMOUNT']  = 'KPB'
    result = result[['MA_NH', 'PRODUCT', 'TERM', 'CURRENCY', 'TYPE_OF_INTEREST',
                     'TYPE', 'TYPE_OF_DEPOSIT', 'SEGMENT','AMOUNT', 'INTEREST', 'DATE']]
    result['INTEREST'] = result['INTEREST'].astype(float)
    result.reset_index(inplace = True, drop = True)
    return result


def clean_agribank(data):
    def map(x):
        if 'Tháng' in x:
            return x.replace(' Tháng', 'M')
        elif 'thanh' in x:
            return 'TGTT'
        else:
            return 'KKH'
    day = date.today().strftime('%d/%m/%Y')
    data.columns = ['x', 'MA_NH', 'TERM', 'VND', 'USD', 'EUR']
    data = data.iloc[1:, :]
    temp = data[['MA_NH', 'TERM', 'VND', 'USD', 'EUR']]
    result = temp.melt(id_vars=['MA_NH', 'TERM'],
                       var_name='CURRENCY', value_name='INTEREST')
    result['TERM'] = result['TERM'].apply(lambda x: map(x))
    result['PRODUCT'] = 'ONE PRODUCT'
    result['TYPE_OF_INTEREST'] = 'TK'
    result['TYPE'] = 'QUAY'
    result['SEGMENT'] = 'KHCN'
    result['DATE'] = day
    result['TYPE_OF_DEPOSIT'] = result['TERM'].apply(
        lambda x: 'KKH' if 'M' not in x else 'CKH')
    result['AMOUNT']  = 'KPB'
    result = result[['MA_NH', 'PRODUCT', 'TERM', 'CURRENCY', 'TYPE_OF_INTEREST',
                     'TYPE', 'TYPE_OF_DEPOSIT', 'SEGMENT','AMOUNT', 'INTEREST', 'DATE']]
    result['INTEREST'] = result['INTEREST'].astype(float)
    result.reset_index(inplace=True, drop=True)
    return result


def clean_klb(data):
    day = date.today().strftime('%d/%m/%Y')
    data.columns = ['x', 'MA_NH', 'TERM', 'CK', '12M', '6M', '3M', '1M', 'DK']
    temp = data[['MA_NH', 'TERM', 'CK', '12M', '6M', '3M', '1M', 'DK']]
    temp = temp.loc[3:, :]
    result = temp.melt(id_vars=['MA_NH', 'TERM'],
                       var_name='TYPE_OF_INTEREST', value_name='INTEREST')
    result.dropna(inplace=True)
    result.reset_index(inplace=True, drop=True)
    result['TERM'] = result['TERM'].apply(lambda x: x.lstrip('0').replace(
        ' tháng', 'M') if 'tháng' in x else x.lstrip('0').replace(' tuần', 'W'))
    result['INTEREST'] = result['INTEREST'].astype('float')
    result['PRODUCT'] = 'ONE PRODUCT'
    result['CURRENCY'] = 'VND'
    result['TYPE'] = 'QUAY'
    result['TYPE_OF_DEPOSIT'] = 'CKH,TK'
    result['SEGMENT'] = 'KHCN'
    result['DATE'] = day
    result['AMOUNT']  = 'KPB'
    result = result[['MA_NH', 'PRODUCT', 'TERM', 'CURRENCY', 'TYPE_OF_INTEREST',
                     'TYPE', 'TYPE_OF_DEPOSIT', 'SEGMENT','AMOUNT', 'INTEREST', 'DATE']]
    result['INTEREST'] = result['INTEREST'].astype(float)
    result.reset_index(inplace=True, drop=True)
    return result


def clean_lvp(data):
    day = date.today().strftime('%d/%m/%Y')
    data.columns = ['x', 'MA_NH', 'TERM', 'DK', '1M', '3M', 'CK', 'USD']
    temp = data[['MA_NH', 'TERM', 'DK', '1M', '3M', 'CK', 'USD']]
    temp = temp.loc[2:, :]
    result = temp.melt(id_vars=['MA_NH', 'TERM'],
                       var_name='TYPE_OF_INTEREST', value_name='INTEREST')
    result['CURRENCY'] = result['TYPE_OF_INTEREST'].apply(
        lambda x: 'USD' if x == 'USD' else 'VND')
    result['TYPE_OF_INTEREST'] = result['TYPE_OF_INTEREST'].apply(
        lambda x: 'CK' if x == 'USD' else x)
    result = result[result['INTEREST'] != '-'].copy()
    result['PRODUCT'] = 'ONE PRODUCT'
    result['TYPE'] = 'QUAY'
    result['TYPE_OF_DEPOSIT'] = 'TK'
    result['SEGMENT'] = 'KHCN'
    result['DATE'] = day
    result['AMOUNT']  = 'KPB'
    result = result[['MA_NH', 'PRODUCT', 'TERM', 'CURRENCY', 'TYPE_OF_INTEREST',
                     'TYPE', 'TYPE_OF_DEPOSIT', 'SEGMENT','AMOUNT', 'INTEREST', 'DATE']]
    result['INTEREST'] = result['INTEREST'].astype(float)
    result.reset_index(inplace=True, drop=True)
    return result


def clean_vietinbank(data):
    day = date.today().strftime('%d/%m/%Y')
    data.columns = ['x', 'MA_NH', 'TERM', 'VND-CN',
                    'USD-CN', 'EUR-CN', 'VND-DN', 'USD-DN', 'EUR-DN']
    temp1 = data[['MA_NH', 'TERM', 'VND-CN', 'USD-CN', 'EUR-CN']].loc[3:, :]
    temp1.columns = ['MA_NH', 'TERM', 'VND', 'USD', 'EUR']
    result1 = temp1.melt(id_vars=['MA_NH', 'TERM'],
                         var_name='CURRENCY', value_name='INTEREST')
    result1['SEGMENT'] = 'KHCN'
    temp2 = data[['MA_NH', 'TERM', 'VND-DN', 'USD-DN', 'EUR-DN']].loc[3:, :]
    temp2.columns = ['MA_NH', 'TERM', 'VND', 'USD', 'EUR']
    result2 = temp2.melt(id_vars=['MA_NH', 'TERM'],
                         var_name='CURRENCY', value_name='INTEREST')
    result2['SEGMENT'] = 'KHDN'
    result = pd.concat([result1, result2])

    def map(x):
        if len(re.findall(r'\d+', x)) == 2:
            lis_ = re.findall(r'\d+', x)
            if 'Từ' in x:
                return f'[{lis_[0]}M:{lis_[1]}M)'
            else:
                return f'({lis_[0]}M:{lis_[1]}M)'
        elif len(re.findall(r'\d+', x)) == 1:
            lis_ = re.findall(r'\d+', x)
            if 'Dưới' in x:
                return f'<{lis_[0]}M'
            elif 'Trên' in x:
                return f'>{lis_[0]}M'
            else:
                return f'{lis_[0]}M'
        else:
            return 'KKH'

    def digit(x):
        lis_ = re.findall(r'\d+\.\d+', x)
        return lis_[0]

    result['TERM'] = result['TERM'].apply(lambda x: map(x))
    result['PRODUCT'] = 'ONE PRODUCT'
    result['TYPE_OF_INTEREST'] = 'CK'
    result['TYPE_OF_DEPOSIT'] = 'CKH'
    result['DATE'] = day
    result['TYPE'] = 'QUAY'
    result.reset_index(inplace=True, drop=True)
    result['AMOUNT']  = 'KPB'
    result = result[['MA_NH', 'PRODUCT', 'TERM', 'CURRENCY', 'TYPE_OF_INTEREST',
                     'TYPE', 'TYPE_OF_DEPOSIT', 'SEGMENT','AMOUNT', 'INTEREST', 'DATE']]
    result['INTEREST'] = result['INTEREST'].apply(
        lambda x: x.replace('\xa0-', '999'))
    result = result[result['INTEREST'] != '999']
    result['INTEREST'] = result['INTEREST'].astype(float)
    result.reset_index(inplace=True, drop=True)
    return result


def clean_ncb(data):
    day = date.today().strftime('%d/%m/%Y')
    data.columns = ['x', 'MA_NH', 'TERM', 'CK', '1M', '3M', '6M', '12M', 'DK']
    temp = data[['MA_NH', 'TERM', 'CK', '1M', '3M', '6M', '12M', 'DK']]
    temp = temp.loc[3:, :]
    result = temp.melt(id_vars=['MA_NH', 'TERM'],
                       var_name='TYPE_OF_INTEREST', value_name='INTEREST')
    result = result[result['INTEREST'].notna()]
    result.reset_index(inplace=True, drop=True)
    result['DATE'] = day
    result['TYPE'] = 'QUAY'
    result['CURRENCY'] = 'VND'
    result['SEGMENT'] = 'KHCN'
    result['PRODUCT'] = 'ONE PRODUCT'
    result['INTEREST'] = result['INTEREST'].astype(float)
    result['TYPE_OF_DEPOSIT'] = result['PRODUCT'].apply(
        lambda x: 'CKH' if 'CKH' in x else 'TK')
    result['TERM'] = result['TERM'].apply(lambda x: x.lstrip('0').replace(
        " Tháng", 'M') if 'Tháng' in x else x.lstrip('0').replace(' Tuần', 'W'))
    result['TERM'] = result['TERM'].apply(
        lambda x: 'KKH' if x == 'Không kỳ hạn' else x)
    result['AMOUNT']  = 'KPB'
    result = result[['MA_NH', 'PRODUCT', 'TERM', 'CURRENCY', 'TYPE_OF_INTEREST',
                     'TYPE', 'TYPE_OF_DEPOSIT', 'SEGMENT','AMOUNT', 'INTEREST', 'DATE']]
    return result


def clean_vietcap(data):
    day = date.today().strftime('%d/%m/%Y')
    data.columns = ['x', 'MA_NH', 'TERM', 'EUR', 'USD', 'VND']
    temp = data[['MA_NH', 'TERM', 'EUR', 'USD', 'VND']]
    temp = temp.loc[0:, :]
    result = temp.melt(id_vars=['MA_NH', 'TERM'],
                       var_name='CURRENCY', value_name='INTEREST')
    result = result[result['INTEREST'].notna()]
    result.reset_index(inplace=True, drop=True)
    result['DATE'] = day
    result['TYPE'] = 'QUAY'
    result['SEGMENT'] = 'KHCN'
    result['PRODUCT'] = 'ONE PRODUCT'
    result['TYPE_OF_INTEREST'] = 'CK'
    result['INTEREST'] = result['INTEREST'].astype(float)
    result['TYPE_OF_DEPOSIT'] = result['PRODUCT'].apply(
        lambda x: 'CKH' if 'CKH' in x else 'TK')
    result['TERM'] = result['TERM'].apply(lambda x: x.lstrip('0').replace(
        " tháng", 'M') if 'tháng' in x else x.lstrip('0').replace(' tuần', 'W'))
    result['TERM'] = result['TERM'].apply(
        lambda x: 'KKH' if x == 'Không kỳ hạn' else x)
    result['AMOUNT']  = 'KPB'
    result = result[['MA_NH', 'PRODUCT', 'TERM', 'CURRENCY', 'TYPE_OF_INTEREST',
                     'TYPE', 'TYPE_OF_DEPOSIT', 'SEGMENT','AMOUNT', 'INTEREST', 'DATE']]
    return result


def clean_nama(data):
    day = date.today().strftime('%d/%m/%Y')
    data.columns = ['x', 'MA_NH', 'TERM', 'CK',
                    '1M', 'DK', '3M', '6M']
    temp = data[['MA_NH', 'TERM', 'CK', '1M', 'DK', '3M', '6M']]
    temp = temp.loc[1:, :]
    result = temp.melt(id_vars=['MA_NH', 'TERM'],
                       var_name='TYPE_OF_INTEREST', value_name='INTEREST')
    result = result[result['INTEREST'].notna()]
    result.reset_index(inplace=True, drop=True)
    result = result[~result['INTEREST'].isin(['-'])]
    result = result[~result['INTEREST'].isin([' '])]
    result['DATE'] = day
    result['TYPE'] = 'QUAY'
    result['SEGMENT'] = 'KHCN'
    result['PRODUCT'] = 'ONE PRODUCT'
    result['CURRENCY'] = 'VND'
    result['TYPE_OF_DEPOSIT'] = 'TK'
    result['INTEREST'] = result['INTEREST'].astype(float)
    result['TERM'] = result['TERM'].apply(lambda x: x.lstrip('0').replace(
        " tháng", 'M') if 'tháng' in x else x.lstrip('0').replace(' tuần', 'W'))
    result['TERM'] = result['TERM'].apply(
        lambda x: 'KKH' if x == 'Không kỳ hạn' else x)
    result['AMOUNT']  = 'KPB'
    result = result[['MA_NH', 'PRODUCT', 'TERM', 'CURRENCY', 'TYPE_OF_INTEREST',
                     'TYPE', 'TYPE_OF_DEPOSIT', 'SEGMENT','AMOUNT', 'INTEREST', 'DATE']]
    return result


def clean_baoviet(data):
    day = date.today().strftime('%d/%m/%Y')
    data.columns = ['x', 'MA_NH', 'TERM', 'DK', 'CK', '1M', '3M']
    temp = data[['MA_NH', 'TERM', 'DK', 'CK', '1M', '3M']]
    temp = temp.loc[1:, :]
    result = temp.melt(id_vars=['MA_NH', 'TERM'],
                       var_name='TYPE_OF_INTEREST', value_name='INTEREST')
    result = result[result['INTEREST'].notna()]
    result.reset_index(inplace=True, drop=True)
    result = result[~result['INTEREST'].isin(['-'])]
    result['DATE'] = day
    result['TYPE'] = 'QUAY'
    result['SEGMENT'] = 'KHCN'
    result['PRODUCT'] = 'ONE PRODUCT'
    result['CURRENCY'] = 'VND'
    result['TYPE_OF_DEPOSIT'] = 'TK'
    result['INTEREST'] = result['INTEREST'].astype(float)
    result['TERM'] = result['TERM'].apply(
        lambda x: x.lstrip('0').replace(" tháng", 'M'))
    result['TERM'] = result['TERM'].apply(lambda x: x.lstrip('0').replace("7 ngày", '1W') if '7 ngày' in x else x.lstrip(
        '0').replace('14 ngày', '2W') if '14 ngày' in x else x.lstrip('0').replace('21 ngày', '3W'))
    result['TERM'] = result['TERM'].apply(
        lambda x: 'KKH' if x == 'Không kỳ hạn' else x)
    result['AMOUNT']  = 'KPB'
    result = result[['MA_NH', 'PRODUCT', 'TERM', 'CURRENCY', 'TYPE_OF_INTEREST',
                     'TYPE', 'TYPE_OF_DEPOSIT', 'SEGMENT','AMOUNT', 'INTEREST', 'DATE']]
    return result


def clean_vcb(data):
    def map(x):
        if 'tháng' in x:
            return x.lstrip('0').replace(" tháng", 'M')
        elif 'ngày' in x:
            return x.lstrip('0').replace(" ngày", 'D')
        else:
            return 'KKH'
    day = date.today().strftime('%d/%m/%Y')
    data.columns = ['x', 'MA_NH', 'TERM', 'VND', 'EUR', 'USD']
    df = data[['MA_NH', 'TERM', 'VND', 'EUR', 'USD']]
    df_tk = df.loc[2:14, :]
    df_ckh = df.loc[16:, :]
    result_tk = df_tk.melt(
        id_vars=['MA_NH', 'TERM'], var_name='CURRENCY', value_name='INTEREST')
    result_tk['TYPE_OF_DEPOSIT'] = 'TK'
    result_ckh = df_ckh.melt(
        id_vars=['MA_NH', 'TERM'], var_name='CURRENCY', value_name='INTEREST')
    result_ckh['TYPE_OF_DEPOSIT'] = 'CKH'
    result = pd.concat([result_tk, result_ckh])
    result['PRODUCT'] = 'ONE PRODUCT'
    result['TYPE_OF_INTEREST'] = 'CK'
    result['SEGMENT'] = 'KHCN'
    result['TYPE'] = 'QUAY'
    result['DATE'] = day
    result['TERM'] = result['TERM'].apply(lambda x: map(x))
    result['AMOUNT']  = 'KPB'
    result = result[['MA_NH', 'PRODUCT', 'TERM', 'CURRENCY', 'TYPE_OF_INTEREST',
                     'TYPE', 'TYPE_OF_DEPOSIT', 'SEGMENT','AMOUNT', 'INTEREST', 'DATE']]
    return result


def clean_sgb(data):
    day = date.today().strftime('%d/%m/%Y')
    data.columns = ['x', 'MA_NH', 'TERM', 'CK', '3M', '1M', 'DK']
    temp = data[['MA_NH', 'TERM', 'CK', '3M', '1M', 'DK']]
    temp = temp.loc[2:, :]
    result = temp.melt(id_vars=['MA_NH', 'TERM'],
                       var_name='TYPE_OF_INTEREST', value_name='INTEREST')
    result = result[result['INTEREST'].notna()]
    result.reset_index(inplace=True)
    result = result.drop(columns='index')
    result = result[~result['INTEREST'].isin([' '])]
    result['DATE'] = day
    result['TYPE'] = 'QUAY'
    result['SEGMENT'] = 'KHCN'
    result['PRODUCT'] = 'ONE PRODUCT'
    result['CURRENCY'] = 'VND'
    result['TYPE_OF_DEPOSIT'] = 'TK'
    result['INTEREST'] = result['INTEREST'].astype(float)
    result['TERM'] = result['TERM'].apply(lambda x: x.lstrip('0').replace(
        " tháng", 'M') if 'tháng' in x else x.lstrip('0').replace(' tuần', 'W'))
    result['TERM'] = result['TERM'].apply(
        lambda x: 'KKH' if x == 'Tiết kiệm không kỳ hạn' else x)
    result['AMOUNT']  = 'KPB'
    result = result[['MA_NH', 'PRODUCT', 'TERM', 'CURRENCY', 'TYPE_OF_INTEREST',
                     'TYPE', 'TYPE_OF_DEPOSIT', 'SEGMENT','AMOUNT', 'INTEREST', 'DATE']]
    return result


def clean_tpb(data):
    day = date.today().strftime('%d/%m/%Y')
    data.columns = ['x', 'MA_NH', 'TERM', 'Đắc Lộc',
                    'An Lộc', 'Điện Tử', 'Bảo Lộc', 'Lĩnh Lãi Cuối Kỳ']
    temp = data[['MA_NH', 'TERM', 'Đắc Lộc', 'An Lộc',
                 'Điện Tử', 'Bảo Lộc', 'Lĩnh Lãi Cuối Kỳ']]
    temp = temp.loc[1:, :]
    result = temp.melt(id_vars=['MA_NH', 'TERM'],
                       var_name='PRODUCT', value_name='INTEREST')
    result = result[result['INTEREST'].notna()]
    result.reset_index(inplace=True)
    result = result.drop(columns='index')
    result = result[~result['INTEREST'].isin([' '])]
    result['DATE'] = day
    result['TYPE'] = 'QUAY'
    result['SEGMENT'] = 'KHCN'
    result['TYPE_OF_INTEREST'] = 'CK'
    result['CURRENCY'] = 'VND'
    result['TYPE_OF_DEPOSIT'] = 'TK'
    result['INTEREST'] = result['INTEREST'].astype(float)
    result['TERM'] = result['TERM'].apply(lambda x: x.lstrip('0').replace(
        " THÁNG", 'M') if 'THÁNG' in x else x.lstrip('0').replace(' tuần', 'W'))
    result['TERM'] = result['TERM'].apply(
        lambda x: 'KKH' if x == 'Không kỳ hạn' else x)
    result['AMOUNT']  = 'KPB'
    result = result[['MA_NH', 'PRODUCT', 'TERM', 'CURRENCY', 'TYPE_OF_INTEREST',
                     'TYPE', 'TYPE_OF_DEPOSIT', 'SEGMENT','AMOUNT', 'INTEREST', 'DATE']]
    return result


def clean_bidv(data):
    day = date.today().strftime('%d/%m/%Y')
    data.columns = ['x', 'MA_NH', 'TERM', 'Tenor', 'USB', 'VND', 'JPY', 'EUR']
    temp = data[['MA_NH', 'TERM', 'Tenor', 'USB', 'VND', 'JPY', 'EUR']]
    temp = temp.loc[1:, :]

    #temp['TERM'] = temp['TERM'].apply(lambda x: x.replace(' tháng','M') if 'tháng' in x else 'KKH')
    result = temp.drop(columns='Tenor').melt(
        id_vars=['MA_NH', 'TERM'], var_name='CURRENCY', value_name='INTEREST')

    result = result[result['INTEREST'].notna()]
    result.reset_index(inplace=True)
    result = result.drop(columns='index')
    result = result[~result['INTEREST'].isin([' '])]
    result['DATE'] = day
    result['TYPE'] = 'QUAY'
    result['SEGMENT'] = 'KHCN'
    result['PRODUCT'] = 'ONE PRODUCT'
    result['TYPE_OF_INTEREST'] = 'DK'
    result['TYPE_OF_DEPOSIT'] = 'TK'
    result['INTEREST'] = result['INTEREST'].astype(float)
    result['TERM'] = result['TERM'].apply(lambda x: x.lstrip('0').replace(
        " Tháng", 'M') if 'Tháng' in x else x.lstrip('0').replace(' tuần', 'W'))
    result['TERM'] = result['TERM'].apply(
        lambda x: 'KKH' if x == 'Không kỳ hạn' else x)
    result['AMOUNT']  = 'KPB'
    result = result[['MA_NH', 'PRODUCT', 'TERM', 'CURRENCY', 'TYPE_OF_INTEREST',
                     'TYPE', 'TYPE_OF_DEPOSIT', 'SEGMENT','AMOUNT', 'INTEREST', 'DATE']]
    return result


def clean_msb(data):
    day = date.today().strftime('%d/%m/%Y')
    data.columns = ['x', 'MA_NH', 'TERM', 'Lãi suất cao nhất',
                    'Định kỳ sinh lời', 'Trả lãi ngay', 'Ong Vàng', 'Măng Non', 'HĐ tiền gửi']
    temp = data[['MA_NH', 'TERM', 'Lãi suất cao nhất', 'Định kỳ sinh lời',
                 'Trả lãi ngay', 'Ong Vàng', 'Măng Non', 'HĐ tiền gửi']]
    temp = temp.loc[1:, :]

    result = temp.melt(id_vars=['MA_NH', 'TERM'],
                       var_name='PRODUCT', value_name='INTEREST')

    result = result[result['INTEREST'].notna()]
    result.reset_index(inplace=True)
    result = result.drop(columns='index')
    result = result[~result['INTEREST'].isin([' '])]
    result['DATE'] = day
    result['TYPE'] = 'QUAY'
    result['SEGMENT'] = 'KHCN'
    result['CURRENCY'] = 'VND'
    result['TYPE_OF_INTEREST'] = 'DK'
    result['TYPE_OF_DEPOSIT'] = 'TK'
    result['INTEREST'] = result['INTEREST'].astype(float)
    result['TERM'] = result['TERM'].apply(lambda x: x.lstrip('0').replace(
        " tháng", 'M') if 'tháng' in x else x.lstrip('0').replace(' năm', 'Y'))
    result['TERM'] = result['TERM'].apply(
        lambda x: 'KKH' if x == 'Rút trước hạn' else x)
    result['AMOUNT']  = 'KPB'
    result = result[['MA_NH', 'PRODUCT', 'TERM', 'CURRENCY', 'TYPE_OF_INTEREST',
                     'TYPE', 'TYPE_OF_DEPOSIT', 'SEGMENT','AMOUNT', 'INTEREST', 'DATE']]
    return result


def clean_acb(data):
    day = date.today().strftime('%d/%m/%Y') 
    data.columns = ['x', 'MA_NH','AMOUNT','[1W:3W)','[1M:2M)','[2M:3M)','[3M:6M)','[6M:9M)','[9M:12M)','12M','RENEW']
    temp       = data.loc[2:,:].copy()
    def map(x):
        x = x.replace(' tỷ','000')
        lis_ = re.findall(r'\d+',x)
        if len(lis_) == 2:
            return f'[{lis_[0]}:{lis_[1]})'
        elif '<' in x:
            return f'<{lis_[0]}'
        else:
            return f'>={lis_[0]}'
    temp.reset_index(inplace = True, drop = True)
    temp['AMOUNT']  = temp['AMOUNT'].apply(lambda x: map(x))
    temp['[1W:3W)'] = temp['[1W:3W)'].fillna(temp.loc[0,'[1W:3W)'])
    temp['RENEW']   = temp['RENEW'].fillna(temp.loc[0,'RENEW'])
    result          = temp.drop(columns = 'x').melt(id_vars = ['MA_NH','AMOUNT'], var_name = 'TERM', value_name = 'INTEREST')
    result['PRODUCT']  = 'ONE PRODUCT'
    result['CURRENCY'] = 'VND'
    result['TYPE_OF_INTEREST'] = 'CK'
    result['TYPE']             = 'ONLINE'
    result['TYPE_OF_DEPOSIT']  = 'CKH'
    result['SEGMENT']          = 'KHCN'
    result['DATE']             = day 
    result['INTEREST']         = result['INTEREST'].astype(float)
    result = result[['MA_NH','PRODUCT','TERM','CURRENCY','TYPE_OF_INTEREST','TYPE','TYPE_OF_DEPOSIT','SEGMENT','AMOUNT','INTEREST','DATE']]  
    return result  


def clean_sacom(data):
    day = date.today().strftime('%d/%m/%Y') 
    data.columns = ['x', 'MA_NH','TERM','CK','3M','1M','DK','USD','EUR','AUD','y']
    data         = data[['MA_NH','TERM','CK','3M','1M','DK','USD','EUR','AUD']].loc[2:,:]
    temp1      = data.drop(columns = ['USD','EUR','AUD']).melt(id_vars = ['MA_NH','TERM'], var_name = 'TYPE_OF_INTEREST', value_name = 'INTEREST')
    temp1.dropna(inplace = True)
    temp1['CURRENCY'] = 'VND'
    temp2      = data.drop(columns = ['CK','3M','1M','DK']).melt(id_vars = ['MA_NH','TERM'], var_name = 'CURRENCY', value_name = 'INTEREST')
    temp2.dropna(inplace = True)
    temp2['TYPE_OF_INTEREST'] = 'CK'
    result = pd.concat([temp1,temp2])
    result.reset_index(inplace = True, drop = True)
    result['PRODUCT'] = 'ONE PRODUCT'
    result['TYPE']    = 'QUAY'
    result['TYPE_OF_DEPOSIT'] = 'TK'
    result['SEGMENT']         = 'KHCN'
    result['AMOUNT']          = 'KPB'
    result['DATE']            = day
    result = result[['MA_NH','PRODUCT','TERM','CURRENCY','TYPE_OF_INTEREST','TYPE','TYPE_OF_DEPOSIT','SEGMENT','AMOUNT','INTEREST','DATE']]  
    result['TERM']            = result['TERM'].apply(lambda x: x.replace(' ','').replace('tháng','M'))
    result['INTEREST']        = result['INTEREST'].astype(float)
    return result


def clean_tcb(data):
    day = date.today().strftime('%d/%m/%Y') 
    th49 = data[['Ma_NH',0,2,3,4]]
    th50 = data[['Ma_NH',0,5,6,7]]
    ut49 = data[['Ma_NH',0,8,9,10]]
    ut50 = data[['Ma_NH',0,11,12,13]]
    def sub_clean_tcb(sub_df,product):
        sub_df.columns = ['MA_NH','TERM','<1000','[1000:3000)','>=3000']
        sub_df         = sub_df.loc[4:,:]
        sub_result  = sub_df.melt(id_vars = ['MA_NH','TERM'], var_name = 'AMOUNT', value_name = 'INTEREST')
        sub_result['TERM'] = sub_result['TERM'].apply(lambda x: '1W-3W' if '1-3W' in x else x)
        sub_result.dropna(inplace = True)
        sub_result['PRODUCT'] = product
        sub_result['TYPE_OF_INTEREST'] = 'CK'
        sub_result['CURRENCY']         = 'VND'
        sub_result.reset_index(inplace = True, drop = True)
        return sub_result
    th49 = sub_clean_tcb(th49,'PHATLOC_KHTHUONG_50-')
    th50 = sub_clean_tcb(th50,'PHATLOC_KHTHUONG_50+')
    ut49 = sub_clean_tcb(ut49,'PHATLOC_KHUT_50-')
    ut50 = sub_clean_tcb(ut50,'PHATLOC_KHUT_50+')
    result1 = pd.concat([th49,th50,ut49,ut50])
    result1.reset_index(inplace = True, drop = True)
    
    result2 = data[['Ma_NH',0,1]]
    result2.columns = ['MA_NH','TERM','INTEREST']
    result2 = result2.loc[4:,:]
    result2['TERM'] = result2['TERM'].apply(lambda x: '1W-3W' if '1-3W' in x else x)
    result2.dropna(inplace = True)
    result2.reset_index(inplace = True, drop = True)
    result2['TYPE_OF_INTEREST'] = 'DK'
    result2['AMOUNT']           = 'KPB'
    result2['PRODUCT']          = 'ONE PRODUCT'
    result2['CURRENCY']         = 'VND'
    
    result3 = data[['Ma_NH',0,14,15]]
    result3.columns = ['MA_NH','TERM','USD','EUR']
    result3         = result3.loc[4:,:]
    result3['TERM'] = result3['TERM'].apply(lambda x: '1W-3W' if '1-3W' in x else x)
    result3         = result3.melt(id_vars = ['MA_NH','TERM'], var_name = 'CURRENCY', value_name = 'INTEREST')
    result3.dropna(inplace = True)
    result3.reset_index(inplace = True, drop = True)
    result3['TYPE_OF_INTEREST'] = 'CK'
    result3['AMOUNT']           = 'KPB'
    result3['PRODUCT']          = 'ONE PRODUCT'
    result = pd.concat([result1,result2,result3])
    result['TYPE']              = 'QUAY'
    result['SEGMENT']           = 'KHCN'
    result['DATE']              = day
    result['TYPE_OF_DEPOSIT']   = 'TK'
    result['INTEREST']          = result['INTEREST'].astype(float)
    result = result[['MA_NH','PRODUCT','TERM','CURRENCY','TYPE_OF_INTEREST','TYPE','TYPE_OF_DEPOSIT','SEGMENT','AMOUNT','INTEREST','DATE']]  
    return result


def clean_vpb(data):
    def map(x):
        x = x.replace(' tỷ','000')
        lis_ = re.findall(r'\d+',x)
        if len(lis_) == 2:
            return f'[{lis_[0]}:{lis_[1]})'
        elif '<' in x:
            return f'<{lis_[0]}'
        else:
            return f'>={lis_[0]}'
    day = date.today().strftime('%d/%m/%Y') 
    int_ = float(data.loc[1,1])
    data.columns = ['x','MA_NH','AMOUNT','1W-3W','1M','2M','3M','4M','5M','6M','7M','8M','9M','10M','11M','12M','13M','15M','18M','24M','36M']
    data         = data.loc[1:,:]
    result     = data.drop(columns = 'x').melt(id_vars = ['MA_NH','AMOUNT'], var_name = 'TERM', value_name = 'INTEREST')
    result['INTEREST']  = result['INTEREST'].astype(float)
    result['INTEREST']  = result.apply(lambda row: int_ if ((row['TERM'] == '1W-3W') and (np.isnan(row['INTEREST']))) else row['INTEREST'], axis = 1)
    result['AMOUNT']    = result['AMOUNT'].apply(lambda x: map(x))
    result['PRODUCT']   = 'ONE PRODUCT'
    result['CURRENCY']  = 'VND'    
    result['TYPE_OF_INTEREST'] = 'CK'
    result['TYPE']             = 'QUAY'
    result['TYPE_OF_DEPOSIT']  = 'TK'
    result['SEGMENT']          = 'KHCN'
    result['DATE']             = day
    result = result[['MA_NH','PRODUCT','TERM','CURRENCY','TYPE_OF_INTEREST','TYPE','TYPE_OF_DEPOSIT','SEGMENT','AMOUNT','INTEREST','DATE']]  
    
    return result


def clean_hdb(data):
    day = date.today().strftime('%d/%m/%Y') 
    data.columns = ['x', 'MA_NH','TERM',1, 'CK', 'DK', '1M', '3M', '6M', '12M', 'USD', 'EUR', 'AUD', 'GBP']
    ls12 = float(data[data[1] == 'LS12 loại 1']['CK'].values[0])
    ls13 = float(data[data[1] == 'LS13 loại 1']['CK'].values[0])
    temp = data[['MA_NH','TERM', 'CK', 'DK', '1M', '3M', '6M', '12M', 'USD', 'EUR', 'AUD', 'GBP']].loc[2:,:]
    temp = temp[temp['TERM'].notna()].copy()
    temp['TERM'] = temp['TERM'].apply(lambda x: x.strip('0').strip().replace(' ngày','D').replace(' tháng','M').replace(' tuần','W'))
    result1 = temp.drop(columns = ['USD', 'EUR', 'AUD', 'GBP']).melt(id_vars = ['MA_NH','TERM'], var_name = 'TYPE_OF_INTEREST', value_name = 'INTEREST')
    result1['CURRENCY'] = 'VND'
    result2 = temp.drop(columns = ['CK', 'DK', '1M', '3M', '6M', '12M']).melt(id_vars = ['MA_NH','TERM'], var_name = 'CURRENCY', value_name = 'INTEREST')
    result2['TYPE_OF_INTEREST'] = 'CK'
    result = pd.concat([result1,result2])
    result = result[result['INTEREST'] != '-'].copy()
    result.dropna(inplace = True)
    result.reset_index(inplace = True, drop = True)
    result['AMOUNT'] = 'KPB'
    result.loc[len(result.index)] = ['79321001','12M','CK',ls12,'VND','>=300000'] 
    result.loc[len(result.index)] = ['79321001','13M','CK',ls13,'VND','>=300000'] 
    result['PRODUCT'] = 'ONE PRODUCT'
    result['TYPE']    = 'QUAY'
    result['TYPE_OF_DEPOSIT'] = 'TK'
    result['SEGMENT']         = 'KHCN'
    result['DATE']            = day
    result['INTEREST']        = result['INTEREST'].astype(float)
    result = result[['MA_NH','PRODUCT','TERM','CURRENCY','TYPE_OF_INTEREST','TYPE','TYPE_OF_DEPOSIT','SEGMENT','AMOUNT','INTEREST','DATE']]  
    return result


def clean_shb(data):
    day = date.today().strftime('%d/%m/%Y') 
    def map(x):
        x    = x.replace(' tỷ','000').strip()
        lis_ = re.findall(r'\d+',x)
        if len(lis_) == 2:
            return f'[{lis_[0]}:{lis_[1]})'
        else:
            return f'<{lis_[0]}'
    data.columns = data.loc[0]
    data.rename(columns = {'Kỳ hạn Hạn mức tiền':'AMOUNT',79348001:'MA_NH'}, inplace = True)
    data           = data.loc[1:,:].copy()
    data['AMOUNT'] = data['AMOUNT'].apply(lambda x: map(x))
    result         = data.drop(columns = 0).melt(id_vars = ['MA_NH','AMOUNT'], var_name = 'TERM', value_name = 'INTEREST')
    result['TERM'] = result['TERM'].apply(lambda x: x.replace('T','M').replace(' ',''))
    result['PRODUCT']  = 'ONE PRODUCT'
    result['CURRENCY'] = 'VND'
    result['TYPE_OF_INTEREST'] = 'CK'
    result['TYPE']             = 'QUAY'
    result['TYPE_OF_DEPOSIT']  = 'TK'
    result['SEGMENT']          = 'KHCN'
    result['DATE']             = day
    result['INTEREST']         = result['INTEREST'].astype(float)

    result = result[['MA_NH','PRODUCT','TERM','CURRENCY','TYPE_OF_INTEREST','TYPE','TYPE_OF_DEPOSIT','SEGMENT','AMOUNT','INTEREST','DATE']]  
    return result


def clean_hsbc(data):
    day = date.today().strftime('%d/%m/%Y')     
    data.columns = ['x','MA_NH','TERM','VND','USD','EUR','GBP','AUD','CAD','JPY','HKD','SGD']
    temp       = data.loc[2:,:].copy()
    temp['TERM'] = temp['TERM'].apply(lambda x: x.replace(' ngày','D').replace(' tháng','M'))
    result       = temp.drop(columns = 'x').melt(id_vars = ['MA_NH','TERM'], var_name = 'CURRENCY', value_name = 'INTEREST')
    result.dropna(inplace = True)
    result['PRODUCT'] = 'ONE PRODUCT'
    result['TYPE_OF_INTEREST'] = 'CK'
    result['TYPE']             = 'QUAY'
    result['TYPE_OF_DEPOSIT']  = 'TK'
    result['SEGMENT']          = 'KHCN'
    result['AMOUNT']           = 'KPB'
    result['DATE']             = day
    result['INTEREST']         = result['INTEREST'].astype(float)

    result = result[['MA_NH','PRODUCT','TERM','CURRENCY','TYPE_OF_INTEREST','TYPE','TYPE_OF_DEPOSIT','SEGMENT','AMOUNT','INTEREST','DATE']]  
    return result


func_map = {'Ocb': clean_ocean, 'Gpbank': clean_gpbank, 
            'Pgbank': clean_pgbank, 'Cbbank': clean_cbbank, 
            'Agribank': clean_agribank, 'Oceanbank': clean_ocean, 
            'Kienlongbank': clean_klb, 'Lienvietpostbank': clean_lvp,
            'Vietinbank': clean_vietinbank, 'Ncb': clean_ncb, 
            'Vietcapitalbank': clean_vietcap, 'NamA': clean_nama, 
            'Baovietbank': clean_baoviet, 'Vietcombank': clean_vcb, 
            'Saigonbank': clean_sgb, 'Tpbank': clean_tpb, 
            'Bidv': clean_bidv, 'MSB': clean_msb,'Acb':clean_acb,
            'SacomBank':clean_sacom,'Techcombank':clean_tcb,'Vpbank':clean_vpb,
            'Hdbank':clean_hdb,'Shb':clean_shb,'HSBC':clean_hsbc}

today = date.today().strftime('%d%m%Y')

def clean_all():
    for key,value in func_map.items():
        try:
            df = pd.read_excel('clean/crl_selenium.xlsx', sheet_name = key)
            result = func_map[key](df)
            with pd.ExcelWriter("clean/result.xlsx", engine='openpyxl',mode='a',if_sheet_exists='replace') as writer: 
                result.to_excel(writer,encoding="utf-8"+u"",sheet_name=f'{key}_{today}')
            print("\033[92m{}\033[00m" .format(f'{key}: ++FINISH++'))
        except:
            print("\033[91m{}\033[00m" .format(f'{key}: ++ERROR++'))
            continue
